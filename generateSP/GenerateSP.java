package generateSP;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

public class GenerateSP {
	public static int dimension = 6;
	public static final int number = 1000000;
	public static int range = 10000000;
	public static String pattern = "anti";
	public static double cortop = 0.03;//0.03
	public static final int corspan = 5;//6
	public static final double antiweight = 1;//1
	public static final double antidelta=(1-antiweight)/dimension;
	public static String filename = pattern + "-D" + dimension + "-R"
			+ range + "-N" + number + ".txt";

	int dim;
	Object[] distri;

	public static void set(int mydim,int myrange,String mypattern){
		dimension=mydim;
		range=myrange;
		pattern=mypattern;
		filename=pattern + "-D" + dimension + "-R"
		+ range + "-N" + number + ".txt";
	}

	public GenerateSP(int dim, Object[] dis) {
		// TODO Auto-generated constructor stub
		this.dim = dim;
		distri = new Object[dim];
		for (int i = 0; i < dim; i++) {
			distri[i] = dis[i];
		}
	}

	public void generate(String filename, int number, String pattern) {
		try {
			RandomAccessFile raf = new RandomAccessFile("SkylineData//"+filename, "rw");
			int i = 0;
			long pos = 0;
			for (i = 0; i < number; i++) {
				int min = 0;
				int max = 0;
				boolean anti = false;
				double delta = 0;
				for (int j = 0; j < this.dim; j++) {// write data first
					String tmp = null;
					int value = 0;

					Uniform u = (Uniform) distri[j];
					if (pattern.equals("in")) {
						value = (u).Random();
					}

					else if (pattern.equals("anti")) {
						if (j == 0) {
							value = (u).Random();
							if (value < (u.max - u.min) * cortop  + u.min) {
								min = u.max - corspan;
								max = u.max;
							} else if (value < (u.max - u.min) / 4 + u.min) {
								min = u.max + 2 * u.min - 2 * value - 1;
								max = u.max;
							} else if (value < (u.max - u.min) * 3 / 4 + u.min) {
								min = u.max + u.min - value - range / 4;
								max = u.max + u.min - value + range / 4;
							} else if (value < u.max - (u.max - u.min) * cortop) {
								min = u.min;
								max = 2 * u.max + u.min - 2 * value + 1;
							} else {
								min = u.min;
								max = u.min + corspan;
							}
						} else {
							if (anti == true) {
								if (Math.random() > antiweight) {
									value  = (u).Random();
								} else {
									value = (int) (Math.random() * (max - min) + min);
									anti = true;
								}
							}
							else{
								if (Math.random() > antiweight+delta) {
									value  = (u).Random();
									delta+=antidelta;
								} else {
									value = (int) (Math.random() * (max - min) + min);
									anti = true;
								}

							}
						}

					}

					else if (pattern.equals("cor")) {
						/*
						 * 第一个数落入 top2.5%，其余的在top5%之内
						 * top2.5%~top25%，其余的在top5%~top50%
						 * top25%~top75%，其余的在正负25%之间
						 * top75%~top97.5%，其余的在top50%~top95%
						 * top97.5%~，其余的在top95%~
						 */
						if (j == 0) {
							value = (u).Random();
                            /* System.out.println("value = " + value); */
							if (value < (u.max - u.min) * cortop + u.min) {
								min = u.min;
								max = (int) (u.min + corspan);
							} else if (value < (u.max - u.min) / 4 + u.min) {
								min = u.min;
								max = value + value - u.min + 1;
							}

							else if (value < (u.max - u.min) * 3 / 4 + u.min) {
								min = value - range / 4;
								max = value + range / 4;
							} else if (value < u.max - (u.max - u.min) * cortop) {
								min = value + value - u.max - 1;
								max = u.max;
							} else {
								min = (int) (u.max - corspan);
								max = u.max;
							}

						} else {
							value = (int) (Math.random() * (max - min) + min);
						}
                        /* if(value < 0) { */
                        /*     System.out.println("Max = " + max + " Min = " + min + " u.max = " + u.max + " u.min = " + u.min + " offset = " + (max - min) + " value = " + value); */
                        /* } */

					}
					tmp = String.valueOf(value);
					raf.writeBytes(tmp);
					pos = pos + tmp.length();

					raf.writeBytes("\t");
					pos++;
				}

				if (i != number - 1) {
					while (pos % 100 <= 97) {// fill in blank
						raf.writeByte(36);
						pos++;
					}
					raf.writeBytes("\r\n");
					pos = pos + 2;
				} else {
					while (pos % 100 <= 99 && pos % 100 != 0) {
						raf.writeByte(36);
						pos++;
					}
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void main(String args[]) throws IOException {
		int []dim={6};
		String []p={"in","cor","anti"};
        /* String []p = {"cor"}; */
		int []ran={10000000};
		for(int k=0;k<dim.length;k++){
		for(int j=0;j<p.length;j++){
			for(int l=0;l<ran.length;l++){
		set(dim[k], ran[l], p[j]);
		Uniform u0 = new Uniform(1, range+1);
		Object[] o = new Object[dimension];
		for (int i = 0; i < dimension; i++) {
			o[i] = u0;
		}
		GenerateSP gsp = new GenerateSP(dimension, o);

		gsp.generate(filename, number, pattern);
		}
		}
	}
	}
}
