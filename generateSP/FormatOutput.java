package generateSP;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

public class FormatOutput {
    String pathname;

    public FormatOutput(String pathname) {
        this.pathname = pathname;
    }

    public void format() throws IOException {
        File file = new File(pathname);
        File out = new File(pathname + ".formatted");

        BufferedReader reader = new BufferedReader(new FileReader(file));
        PrintWriter pw = new PrintWriter(out);

        String line = null;
        int num = 1;
        while((line = reader.readLine()) != null) {

            // reformat each line to - <lineNum dim1 dim2 ...>
            String []tokens = line.split("\t");
            StringBuilder sb = new StringBuilder();
            sb.append(String.valueOf(num));
            sb.append(" ");

            int i = 0;
            int val = 0;
            for(; i < tokens.length - 2; i++) {
                val = Integer.parseInt(tokens[i]);
                sb.append(String.valueOf((val/1000.0)));
                sb.append(",");
            }
            val = Integer.parseInt(tokens[i]);
            sb.append(String.valueOf((val/1000.0)));

            //write the formated line to a file
            pw.println(sb.toString());

            num++;

        }

        pw.close();
        reader.close();
    }

    public static void main(String[] args) {
        if(args.length != 1) {
            System.out.println("Invalid Input Format: one argument required!");
        }

        String pathname = args[0];
        FormatOutput fo = new FormatOutput(pathname);
        try {
            fo.format();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

