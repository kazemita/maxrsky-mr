package generateSP;

public class Normal implements Distribution {
	// 数学期望
	double miu;

	// 方差
	double sigma2;

	Normal(double miu, double sigma2) {
		this.miu = miu;
		this.sigma2 = sigma2;
	}

	public int Random() {
		int N = 12;
		double x = 0, temp = N;
		x = 0;
		for (int i = 0; i < N; i++) {
			x = x + (Math.random());
		}
		x = (x - temp / 2) / (Math.sqrt(temp / 12));
		x = miu + x * Math.sqrt(sigma2);
		return (int) x;
	}

	public static void main(String args[]) {
		Normal r = new Normal(-1000.0, 2000);
		for (int i = 0; i < 100; i++) {
			System.out.println(r.Random());

		}
	}
}
