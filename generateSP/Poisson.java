package generateSP;

public class Poisson implements Distribution {
	double lamda;

	public Poisson(double lamda) {
		// TODO Auto-generated constructor stub
		this.lamda = lamda;
	}

	@Override
	public int Random() {
		// TODO Auto-generated method stub
		double x = 0, b = 1, c = Math.exp(-this.lamda), u;
		do {
			u = Math.random();
			b *= u;
			if (b >= c)
				x++;
		} while (b >= c);
		return (int) x;
	}
	
	public static void main(String args[]){
		Poisson p=new Poisson(100.0);
		for(int i=0;i<100;i++){
			System.out.println(p.Random());
		}
	}

}
