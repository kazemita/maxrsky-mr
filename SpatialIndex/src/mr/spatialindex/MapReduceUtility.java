package mr.spatialindex;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import spatialindex.rtree.Box;
import spatialindex.rtree.Point;
import spatialindex.ssr.DSLObject;

public class MapReduceUtility {
	public static DSLObject getPointsFromString(String data, int dimension) {
		String[] fields = data.split("\t");
		Long id = Long.parseLong(fields[0]);
		
		String[] pset = getIdDataPairString(fields[1]);
		String strOrigPoint = pset[0];
		String[] strPoints = pset[1].split(";");
		String[] strValues = null;
		DSLObject dslObj = new DSLObject();

		// the last one has no object
		Point[] points = new Point[strPoints.length];
		Point origPoint = new Point(dimension);

		// original points
		strValues = strOrigPoint.split(",");
		for (int i = 0; i < dimension; i++) {
			origPoint.vals[i] = Float.valueOf(strValues[i]);
		}

		for (int i = 0; i < strPoints.length; i++) {
			strValues = strPoints[i].split(",");

			if (strValues.length != dimension)
				return null;

			points[i] = new Point(dimension);
			for (int j = 0; j < dimension; j++) {
				points[i].vals[j] = Float.valueOf(strValues[j]);
			}

		}

		dslObj.points = points;
		dslObj.origPoint = origPoint;
		dslObj.id = id;

		return dslObj;
	}

	public static Point getCenterFromString(String data, int dimension) {
		String[] fields = data.split("\t");
		String strOrigPoint = fields[0];
		Point origPoint = new Point(dimension);

		String[] strValues = strOrigPoint.split(",");
		for (int i = 0; i < dimension; i++) {
			origPoint.vals[i] = Float.valueOf(strValues[i]);
		}

		return origPoint;
	}

	public static Box getBoxFromString(String data, int dimension) {
		Box b = new Box(dimension);
		String[] boxString = data.split("\t");
		String[] fields = boxString[1].split(",");
		for (int i = 0; i < dimension; i++) {
			b.lowerBounds[i] = Float.valueOf(fields[i * 2]);
			b.upperBounds[i] = Float.valueOf(fields[i * 2 + 1]);
		}

		b.id = Long.valueOf(boxString[0]);

		return b;
	}

	public static Box getBoxFromDSL(DSLObject obj, int dimension) {
		Box mbr = new Box(dimension);
		mbr.setMinMax();

		for (int i = 0; i < obj.points.length; i++) {
			for (int d = 0; d < dimension; d++) {
				mbr.upperBounds[d] = Math.max(obj.points[i].vals[d],
						mbr.upperBounds[d]);
			}
		}

		for (int d = 0; d < dimension; d++) {
			mbr.lowerBounds[d] = -mbr.upperBounds[d];

			mbr.lowerBounds[d] += obj.origPoint.vals[d];
			mbr.upperBounds[d] += obj.origPoint.vals[d];
		}
		
		mbr.id = obj.id;

		return mbr;
	}

	public static String formatRTreeMBR(Box mbr) {
		StringBuilder sb = new StringBuilder((mbr.dimension + 1)* 10);
		sb.append(mbr.id);
		sb.append(",");
		for (int i = 0; i < mbr.dimension; i++) {
			sb.append(mbr.lowerBounds[i]);
			sb.append(",");
			sb.append(mbr.upperBounds[i]);
			sb.append(",");
		}
		
		sb.deleteCharAt(sb.length() - 1);
		
		return sb.toString();
	}

	public static ArrayList<Box> getBoxesFromDSL(DSLObject dslObj, int numBox,
			int dimension) {
		ArrayList<Box> mbrs = new ArrayList<>();
		ArrayList<Point> pointsAry = new ArrayList<>();
		// exclude world space original point and add local space original point
		Point orig = new Point(dimension);
		for (int i = 0; i < dimension; i++) {
			orig.vals[i] = 0.0f;
		}
		pointsAry.add(orig);
		for (int i = 0; i < dslObj.points.length; i++) {
			pointsAry.add(dslObj.points[i]);
		}
		// sort them
		Collections.sort(pointsAry);
		// pick from the smallest one
		// compute number of Points per group
		int numPointPerGroup = (pointsAry.size() + numBox - 1) / numBox;

		for (int b = 0; b < numBox; b++) {
			int idxStart = Math.max(b * numPointPerGroup - 1, 0);
			int idxEnd = Math.min(b * numPointPerGroup + numPointPerGroup,
					pointsAry.size());

			Box mbr = new Box(dimension);
			mbr.setMinMax();
			// compute the mbr for the box denoted by current point and previous
			for (int idx = idxStart; idx < idxEnd; idx++) {
				for (int i = 0; i < dimension; i++) {
					mbr.upperBounds[i] = Math.max(mbr.upperBounds[i],
							pointsAry.get(idx).vals[i]);

					mbr.lowerBounds[i] = Math.min(mbr.lowerBounds[i],
							pointsAry.get(idx).vals[i]);
				}
			}

			mbrs.add(mbr);
		}
		// unfolding them
		for (int i = 0; i < dimension; i++) {
			ArrayList<Box> synMBRs = new ArrayList<>();
			for (Box b : mbrs) {
				Box synB = b.copy();
				synB.upperBounds[i] = Math.max(-b.upperBounds[i],
						-b.lowerBounds[i]);
				synB.lowerBounds[i] = Math.min(-b.upperBounds[i],
						-b.lowerBounds[i]);
				synMBRs.add(synB);
			}

			for (Box b : synMBRs) {
				mbrs.add(b);
			}
		}

		for (Box b : mbrs) {
			for (int i = 0; i < dimension; i++) {
				b.upperBounds[i] += dslObj.origPoint.vals[i];
				b.lowerBounds[i] += dslObj.origPoint.vals[i];
			}
			b.id = dslObj.id;
		}

		return mbrs;
	}

	public static HashMap<String, String> readConfigFile(String filename,
			FileSystem fs) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(
				fs.open(new Path(filename))));
		String line = null;
		HashMap<String, String> config = new HashMap<>();
		while ((line = br.readLine()) != null) {
			String[] kv = line.split(":");
			config.put(kv[0].trim(), kv[1].trim());
		}

		br.close();

		return config;

	}

	public static String[] getIdDataPairString(String data) {
		return data.split("@");
	}
}
