package mr.spatialindex;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URI;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.TreeMap;
import java.util.Map;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import spatialindex.rtree.FileSystemSelector;
import spatialindex.rtree.LocalFSManager;
import spatialindex.rtree.RTree;
import spatialindex.rtree.RTreeMemory;
import spatialindex.rtree.SystemLogger;

public class SpatialIndex {

	public static FileSystem getS3FileSystem(Configuration conf)
			throws IOException {
		return FileSystem.get(URI.create("s3n://dbprj/"), conf);
		//return FileSystem.get(conf);
	}

	public static FileSystem getRemoteS3FileSystem(Configuration conf)
			throws IOException {
		return FileSystem.get(URI.create("s3n://ec2skylinetest/"), conf);
		// return FileSystem.get(conf);
	}

	public static FileSystem getHDFSFileSystem(Configuration conf)
			throws IOException {
		return FileSystem.get(URI.create("hdfs:///"), conf);
		// return FileSystem.get(conf);
	}

	public static void testRTreeI() {
		Configuration conf = new Configuration();
		ArrayList<String> records = new ArrayList<>();
		try {
			FileSystem fs = getS3FileSystem(conf);
			BufferedReader br = new BufferedReader(new InputStreamReader(
					fs.open(new Path("/rect-grid-2d"))));
			String line = null;
			while ((line = br.readLine()) != null) {
				records.add(line);
			}

			br.close();

			RTreeMemory rTree = new RTreeMemory();
			rTree.construct(records, 2);
			ByteBuffer bb = rTree.buffer();
			byte[] bbary = new byte[bb.remaining()];
			bb.get(bbary);

			FSDataOutputStream fsdos = fs.create(new Path("/ridx"));
			fsdos.write(bbary);
			fsdos.close();

		} catch (FileNotFoundException ex) {
			SystemLogger.error(ex);
		} catch (IOException ex) {
			SystemLogger.error(ex);
		}

	}

	public static void testRTree() {
		try {
			Configuration conf = new Configuration();
			LocalFSManager fs = new LocalFSManager();
			FileSystem pfs = getS3FileSystem(conf);
			AWSLogger awsLogger = new AWSLogger("/log-rtree", conf);
			// mkdir
			String dir = "/home/hadoop/ridx";
			fs.mkdir(dir);
			FSDataInputStream fsdis = pfs.open(new Path("/rect-grid-0"));
			DataOutputStream dos = new DataOutputStream(new FileOutputStream(
					dir + "/rect-grid-0"));
			int bufferSize = 1024 * 1024;
			byte[] data = new byte[bufferSize];
			while (true) {
				int offset = 0;
				while (offset < bufferSize) {
					int readLen = fsdis.read(data, offset, bufferSize - offset);
					if (readLen > 0)
						offset += readLen;
					else
						break;
				}

				if (offset > 0)
					dos.write(data, 0, offset);
				else
					break;
			}

			fsdis.close();
			dos.close();

			FileSystemSelector.config(FileSystemSelector.LOCAL_FILESYSTEM_TYPE);
			SystemLogger.setWriter(awsLogger.getWriter());
			RTree rTree = new RTree(2);
			rTree.construct(dir + "/rect-grid", 1 << 22);
			awsLogger.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void computeMBR(String configFilePath, String inputFilePath) {
		SystemLogger.info("compute MBR starts");
		try {
			FileSystem rfs = getRemoteS3FileSystem(new Configuration());
			FileStatus[] files = rfs.listStatus(new Path(inputFilePath));
			String interWorkingDir = "s3n://dbprj/MBRs/";

			int idx = 0;
			for (FileStatus file : files) {
				if (file.getLen() > 0) {
					System.out.println("adress file "
							+ file.getPath().toString());
					ComputeMBRMapReduce.work(idx, configFilePath, file
							.getPath().toString(), interWorkingDir + "mbr"
							+ idx);
					idx++;
				}
			}
			SystemLogger
					.info("compute MBR ends. Move the results to one folder");
			FileSystem lfs = getS3FileSystem(new Configuration());
			files = lfs.listStatus(new Path(interWorkingDir));
			for (FileStatus dir : files) {
				if (dir.isDirectory()) {
					FileStatus[] dataFiles = lfs.listStatus(dir.getPath());
					for (FileStatus file : dataFiles) {
						String newFilename = String.format("%s%s-%s",
								interWorkingDir, dir.getPath().getName(), file
										.getPath().getName());
						SystemLogger.info("move File from "
								+ file.getPath().toString() + " to "
								+ newFilename);
						lfs.rename(file.getPath(), new Path(newFilename));
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace(System.err);
		}
		SystemLogger.info("file move end");

	}

	public static String combineSplitPoints(String outputFileFolder) {
		try {
			FileSystem fs = getS3FileSystem(new Configuration());
			ArrayList<String> resultFiles = new ArrayList<>();
			TreeMap<Long, Long> splitPoints = new TreeMap<>();

			for (FileStatus file : fs.listStatus(new Path(outputFileFolder))) {
				System.out.println("\033[33;1m" + file.getPath().toString()
						+ "\033[0m");
				if (file.getPath().getName().indexOf("part-r") == 0) {
					resultFiles.add(file.getPath().toString());
				}
			}

			for (String filename : resultFiles) {
				BufferedReader br = new BufferedReader(new InputStreamReader(
						fs.open(new Path(filename))));
				String line = null;
				while ((line = br.readLine()) != null) {
					String[] fields = line.split("\t");
					splitPoints.put(Long.valueOf(fields[0]),
							Long.valueOf(fields[1]));
				}
				br.close();
			}

			String newFilename = outputFileFolder + "/sp-res";
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(
					fs.create(new Path(newFilename))));

			for (Map.Entry<Long, Long> kvp : splitPoints.entrySet()) {
				bw.write(String.format("%d\t%d\n", kvp.getKey(), kvp.getValue()));
			}

			bw.close();

			return newFilename;
		} catch (IOException e) {
			SystemLogger.error(e);
			return null;
		}
	}

	public static void main(String[] args) {
		String configFilePath = null;
		String inputFilePath = null;
		String outputFileFolder = null;
		int bNeedComputeMBR = 0;
		
		if (args.length == 4) {
			configFilePath = args[0];
			inputFilePath = args[1];
			outputFileFolder = args[2];
			bNeedComputeMBR = Integer.valueOf(args[3]);
		} else if ( args.length == 3) {
			configFilePath = args[0];
			inputFilePath = args[1];
			outputFileFolder = args[2];
		} else {
			System.out.println("at least 3 arguments are needed");
			return;
		}

		if (bNeedComputeMBR == 1) {
			computeMBR(configFilePath, inputFilePath);
			args[1] = "s3n://dbprj/MBRs";
		}

		// String spResFilePath = "s3n://dbprj/1milData/sp-res";
		SystemLogger.info("partition starts");
		// compute split point
		try {
			PartitionMapReduce.work(args);
		} catch (Exception e) {
			e.printStackTrace(System.err);
			return;
		}

		String spResFilePath = combineSplitPoints(outputFileFolder);
		if (spResFilePath == null) {
			System.err.println("sp file combining failed");
			return;
		} else {
			System.out.println("begin rtree construction");
		}

		SystemLogger.info("r-tree construction start");
		// split data and construct r-tree
		try {
			RTreeMapReduce.work(configFilePath, spResFilePath, inputFilePath,
					outputFileFolder + "-rtree");
		} catch (Exception e) {
			e.printStackTrace(System.err);
			return;
		}

		SystemLogger.info("r-tree construction end");
	}
}
