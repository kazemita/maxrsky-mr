package mr.spatialindex;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URI;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import spatialindex.rtree.Box;
import spatialindex.rtree.Point;

public class DatasetConverter {
	private static Box boundary;

	private static Point[] getPointsFromString(String data, int dimension) {
		String[] fields = data.split("\t");
		String strOrigPoint = fields[0];
		String[] strPoints = fields[1].split(";");
		String[] strValues = null;

		Point[] points = new Point[strPoints.length];
		Point origPoint = new Point(dimension);

		// original points
		strValues = strOrigPoint.split(",");
		for (int i = 0; i < dimension; i++) {
			origPoint.vals[i] = Float.valueOf(strValues[i]);
		}

		for (int i = 0; i < strPoints.length - 1; i++) {
			strValues = strPoints[i].split(",");

			if (strValues.length != dimension)
				return null;

			points[i] = new Point(dimension);
			for (int j = 0; j < dimension; j++) {
				points[i].vals[j] = Float.valueOf(strValues[j]);
			}

		}

		points[strPoints.length - 1] = origPoint;

		return points;
	}

	private static String getOutputString(Point[] points, long id, int dimension) {
		Box mbr = new Box(dimension);
		Point origPoint = points[points.length - 1];

		mbr.setMinMax();
		for (int i = 0; i < points.length - 1; i++) {
			Point tmpPD = new Point(points[i].vals);
			Point tmpP1 = new Point(dimension);
			Point tmpP2 = new Point(dimension);

			for (int j = 0; j < dimension; j++) {
				tmpPD.vals[j] -= origPoint.vals[j];
			}

			for (int j = 0; j < dimension; j++) {
				tmpP1.vals[j] = origPoint.vals[j] + tmpPD.vals[j];
				tmpP2.vals[j] = origPoint.vals[j] - tmpPD.vals[j];
			}

			for (int j = 0; j < dimension; j++) {
				mbr.lowerBounds[j] = Math
						.min(tmpP1.vals[j], mbr.lowerBounds[j]);
				mbr.lowerBounds[j] = Math
						.min(tmpP2.vals[j], mbr.lowerBounds[j]);

				mbr.upperBounds[j] = Math
						.max(tmpP1.vals[j], mbr.upperBounds[j]);
				mbr.upperBounds[j] = Math
						.max(tmpP2.vals[j], mbr.upperBounds[j]);
			}
		}

		for (int j = 0; j < dimension; j++) {
			boundary.lowerBounds[j] = Math.min(mbr.lowerBounds[j],
					boundary.lowerBounds[j]);
			boundary.upperBounds[j] = Math.max(mbr.upperBounds[j],
					boundary.upperBounds[j]);
		}

		StringBuilder sb = new StringBuilder();
		sb.append(id);
		sb.append(",");
		for (int j = 0; j < dimension; j++) {
			sb.append(mbr.lowerBounds[j]);
			sb.append(",");

			sb.append(mbr.upperBounds[j]);
			sb.append(",");
		}

		sb.deleteCharAt(sb.length() - 1);

		return sb.toString();
	}

	public static void main(String[] args) {
		try {
			FileSystem fsi = FileSystem.get(
					URI.create("s3n://ec2skylinetest/"), new Configuration());
			FileSystem fso = FileSystem.get(URI.create("s3n://dbprj/"),
					new Configuration());
			String filePath = args[0];
			
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(
					fso.create(new Path("s3n://dbprj/" + args[1]))));

			FileStatus[] files = fsi.listStatus(new Path("s3n://ec2skylinetest/" + filePath));
			int dimension = Integer.valueOf(args[2]);
			long lid = 0;
			
			boundary = new Box(dimension);
			boundary.setMinMax();
			
			for (FileStatus file : files) {
				BufferedReader br = new BufferedReader(new InputStreamReader(
						fsi.open(file.getPath())));
				
				String line = null;
				while ((line = br.readLine()) != null) {
					Point[] points = getPointsFromString(line, dimension);
					bw.write(getOutputString(points, lid, dimension) + "\n");
					lid++;
				}
				
				br.close();
				System.out.println("file " + file.getPath().toString() +" done");
				if (lid % 10000 == 0) {
					System.out.println(String.valueOf(lid) +" records are done");
				}
			}			
			bw.close();
			
			System.out.println(boundary.toString());
		} catch (IOException ioe) {

		}
	}
}
