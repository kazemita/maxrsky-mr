package mr.spatialindex;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Collections;
import java.security.SecureRandom;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import spatialindex.rtree.Point;
import spatialindex.sfcf.SFCFSceneMapper;

public class PartitionMapReduce {
	public static class PMapper extends
			Mapper<Object, Text, LongWritable, LongWritable> {

		private SFCFSceneMapper sfcf = null;
		private int dimension;
		private int numOfPartitions = 64;
		private SecureRandom sr = new SecureRandom();

		private long computeSFCFValue(String data) throws Exception {
			double[] centers = new double[dimension];
			String[] strPair = MapReduceUtility.getIdDataPairString(data);
			Point orig = MapReduceUtility.getCenterFromString(strPair[1], dimension);
			for (int i = 0; i < dimension; i++) {
				centers[i] = orig.vals[i];
			}

			return sfcf.getZOrderValue(centers);
		}

		private double getRandomNum() {
			byte[] rand = new byte[8];
			sr.nextBytes(rand);

			long value = 0;
			for (int i = 0; i < rand.length; i++) {
				value += ((long) rand[i] & 0xffL) << (8 * i);
			}

			return (double) Math.abs(value) / (double) Long.MAX_VALUE;
		}

		@Override
		public void setup(Context context) throws IOException,
				InterruptedException {
			Configuration conf = context.getConfiguration();
			FileSystem fs = SpatialIndex.getS3FileSystem(conf);
			HashMap<String, String> config = MapReduceUtility.readConfigFile(
					conf.get("conf_path"), fs);

			String[] lbs = config.get("lowerBounds").split(",");
			String[] ubs = config.get("upperBounds").split(",");

			dimension = Integer.valueOf(config.get("dimension"));
			numOfPartitions = Integer.valueOf(config.get("numPartitions"));
			sfcf = new SFCFSceneMapper(dimension);

			double[] dlbs = new double[dimension];
			double[] dubs = new double[dimension];
			int[] ncs = new int[dimension];
			for (int i = 0; i < dimension; i++) {
				dlbs[i] = Double.valueOf(lbs[i]);
				dubs[i] = Double.valueOf(ubs[i]);
				ncs[i] = Integer.valueOf(config.get("numCells"));
			}

			sfcf.setParams(dlbs, dubs, ncs);
		}

		@Override
		public void map(Object key, Text value, Context context)
				throws IOException, InterruptedException {
			if (getRandomNum() > 0.02)
				return;

			try {
				LongWritable val = new LongWritable(
						computeSFCFValue(value.toString()));
				context.write(new LongWritable(numOfPartitions), val);
				System.out.printf("%d,%d\n", numOfPartitions, val.get());
			} catch (Exception e) {
				System.err.println(e.getMessage());
				e.printStackTrace(System.err);
			}
		}
	}

	public static class PReducer extends
			Reducer<LongWritable, LongWritable, LongWritable, LongWritable> {

		public void reduce(LongWritable key, Iterable<LongWritable> values,
				Context context) throws IOException, InterruptedException {
			ArrayList<Long> vals = new ArrayList<>();

			for (LongWritable v : values) {
				vals.add(v.get());
			}

			System.out.printf("\033[33;1mTotally %d samples.\033[0m\n",
					vals.size());
			Collections.sort(vals);

			int numOfPartitions = (int) key.get();
			int partitionSize = (int) Math.ceil(vals.size()
					/ (double) numOfPartitions);

			for (int i = partitionSize, idx = 0; i < vals.size()
					&& idx < numOfPartitions - 1; i += partitionSize, idx++) {
				context.write(new LongWritable(idx),
						new LongWritable(vals.get(i)));
			}
		}
	}

	public static void work(String[] args) throws Exception {
		Configuration conf = new Configuration();

		if (args.length != 3) {
			System.err.println("Usage: <con <in> <out>");
			System.exit(2);
		}

		conf.set("conf_path", args[0]);

		Job job = Job.getInstance(conf, "Paritition");
		job.setJarByClass(PartitionMapReduce.class);
		job.setMapperClass(PMapper.class);
		job.setReducerClass(PReducer.class);

		job.setOutputKeyClass(LongWritable.class);
		job.setOutputValueClass(LongWritable.class);

		job.setMapOutputKeyClass(LongWritable.class);
		job.setMapOutputValueClass(LongWritable.class);

		FileInputFormat.addInputPath(job, new Path(args[1]));
		FileOutputFormat.setOutputPath(job, new Path(args[2]));
		// /System.exit( ? 0 : 1);
		job.waitForCompletion(true);
	}
}
