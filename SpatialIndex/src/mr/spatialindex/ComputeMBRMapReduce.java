package mr.spatialindex;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import spatialindex.rtree.Box;
import spatialindex.ssr.DSLObject;

public class ComputeMBRMapReduce {

	public static class CMMapper extends
			Mapper<LongWritable, Text, LongWritable, Text> {

		private int dimension = 2;
		private int boxCount = 3;

		@Override
		public void setup(Context context) throws IOException,
				InterruptedException {
			Configuration conf = context.getConfiguration();
			dimension = conf.getInt("dimension", 4);
			boxCount = conf.getInt("box_count", 3);
		}

		@Override
		public void map(LongWritable key, Text value, Context context)
				throws IOException, InterruptedException {

			DSLObject dslObj = MapReduceUtility.getPointsFromString(
					value.toString(), dimension);
			ArrayList<Box> mbrs = MapReduceUtility.getBoxesFromDSL(dslObj,
					boxCount, dimension);

			int globalID = context.getConfiguration().getInt("data_id", 0);
			long newID = key.get() | ((long) globalID << 48L);

			for (Box b : mbrs) {
				StringBuilder sb = new StringBuilder(256);
				sb.append(newID);
				sb.append(",");
				for (int i = 0; i < dimension; i++) {
					sb.append(b.lowerBounds[i]);
					sb.append(",");
					sb.append(b.upperBounds[i]);
					sb.append(",");
				}
				sb.deleteCharAt(sb.length() - 1);
				context.write(new LongWritable(newID), new Text(sb.toString()));
			}
		}
	}

	public static class CMReducer extends
			Reducer<LongWritable, Text, LongWritable, Text> {

		public void reduce(LongWritable key, Iterable<Text> values,
				Context context) throws IOException, InterruptedException {

			for (Text txt : values) {
				context.write(key, txt);
			}
		}
	}

	public static void work(Integer id, String configFile, String inputDir,
			String outputDir) throws Exception {
		Configuration conf = new Configuration();

		FileSystem fs = SpatialIndex.getS3FileSystem(conf);
		HashMap<String, String> config = MapReduceUtility.readConfigFile(
				configFile, fs);

		conf.setInt("dimension", Integer.valueOf(config.get("dimension")));
		conf.setInt("box_count", Integer.valueOf(config.get("numMBR")));
		conf.setInt("data_id", id);

		Job job = Job.getInstance(conf, "Make mbr");
		job.setJarByClass(PartitionMapReduce.class);
		job.setMapperClass(CMMapper.class);
		job.setReducerClass(CMReducer.class);

		job.setOutputKeyClass(LongWritable.class);
		job.setOutputValueClass(Text.class);

		job.setMapOutputKeyClass(LongWritable.class);
		job.setMapOutputValueClass(Text.class);

		FileInputFormat.addInputPath(job, new Path(inputDir));
		FileOutputFormat.setOutputPath(job, new Path(outputDir));
		// /System.exit( ? 0 : 1);
		job.waitForCompletion(true);
	}
}
