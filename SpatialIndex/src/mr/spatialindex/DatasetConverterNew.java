package mr.spatialindex;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URI;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

public class DatasetConverterNew {
	public static void main(String[] args) {
		try {
			FileSystem fsi = FileSystem.get(
					URI.create("s3n://ec2skylinetest/"), new Configuration());
			FileSystem fso = FileSystem.get(URI.create("s3n://dbprj/"),
					new Configuration());
			String filePath = args[0];

			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(
					fso.create(new Path("s3n://dbprj/" + args[1]))));

			FileStatus[] files = fsi.listStatus(new Path(
					"s3n://ec2skylinetest/" + filePath));
			long lid = 0;
			
			System.out.println(filePath);

			for (FileStatus file : files) {
				BufferedReader br = new BufferedReader(new InputStreamReader(
						fsi.open(file.getPath())));

				String line = null;
				while ((line = br.readLine()) != null) {
					bw.write(String.valueOf(lid));
					bw.write("@");
					bw.write(line + "\n");
					lid++;
				}

				br.close();
				System.out.println("file " + file.getPath().toString()
						+ " done");
				if (lid % 10000 == 0) {
					System.out.println(String.valueOf(lid)
							+ " records are done");
				}
			}
			bw.close();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}
}
