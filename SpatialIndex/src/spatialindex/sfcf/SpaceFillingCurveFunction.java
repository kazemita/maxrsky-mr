/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spatialindex.sfcf;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author PowerWBH
 */
public class SpaceFillingCurveFunction {

	private static long space1Split(long number) {
		long x = number & 0x00000000ffffffffL;

		x = (x | (x << 16)) & 0x0000ffff0000ffffL;
		x = (x | (x << 8)) & 0x00ff00ff00ff00ffL;
		x = (x | (x << 4)) & 0x0f0f0f0f0f0f0f0fL;
		x = (x | (x << 2)) & 0x3333333333333333L;
		x = (x | (x << 1)) & 0x5555555555555555L;

		return x;
	}

	private static long space2Split(long number) {
		long x = number & 0x00000000001fffffL;

		x = (x | (x << 32)) & 0x001f00000000ffffL;
		x = (x | (x << 16)) & 0x001f0000ff0000ffL;
		x = (x | (x << 8)) & 0x100f00f00f00f00fL;
		x = (x | (x << 4)) & 0x10c30c30c30c30c3L;
		x = (x | (x << 2)) & 0x1249249249249249L;

		return x;
	}

	private static long space3Split(long number) {
		long x = number & 0x000000000000ffffL;

		x = (x | (x << 24)) & 0x000000ff000000ffL;
		x = (x | (x << 12)) & 0x000f000f000f000fL;
		x = (x | (x << 6)) & 0x0303030303030303L;
		x = (x | (x << 3)) & 0x1111111111111111L;

		return x;
	}

	public static long zorderFunc(long x, long y) {
		return (space1Split(y) << 1) | space1Split(x);
	}

	public static long zorderFunc(long x, long y, long z) {
		return (space2Split(z) << 2) | (space2Split(y) << 1) | space2Split(x);
	}

	public static long zorderFunc(long x, long y, long z, long w) {
		return (space3Split(w) << 3) | (space3Split(z) << 2) | (space3Split(y) << 1) | space3Split(x);
	}

	public static long zorderFunc(long[] nums) {
		int perNumSize = 64 / nums.length;
		long x = 0;
		for (int i = nums.length - 1; i >= 0; i--) {
			for (int j = perNumSize - 1; j >= 0; j--) {
				x |= ((nums[i] >>> j) & 1) << (j * nums.length + i);
			}
		}

		return x;
	}

	public static long zorderFuncOpt(long[] nums) {
		switch (nums.length) {
			case 2:
				return zorderFunc(nums[0], nums[1]);
			case 3:
				return zorderFunc(nums[0], nums[1], nums[2]);
			case 4:
				return zorderFunc(nums[0], nums[1], nums[2], nums[3]);
			default:
				return zorderFunc(nums);
		}
	}

	public static void printBinary(long x) {
		for (int i = 63; i >= 0; i--) {
			System.out.print((x >> i) & 1);
			if (i % 4 == 0) {
				System.out.print(" ");
			}
		}

		System.out.println();
	}

	public static void testAll() {
		ArrayList<Long> nums = new ArrayList<>();

		for (int i = 0; i < 10000; i++) {
			nums.add(Long.valueOf(i) + 1L);
		}
		// test for 2D
		long cnt = 0L;
		System.out.printf("%s\n", "Test SFCF for 2D");
		for (Long x : nums) {
			for (Long y : nums) {
				long[] l = {x, y};
				long z1 = SpaceFillingCurveFunction.zorderFunc(x, y);
				long z2 = SpaceFillingCurveFunction.zorderFunc(l);
				if (z1 != z2) {
					try {
						System.out.printf("%d\t%d\n", x, y);
						SpaceFillingCurveFunction.printBinary(z1);
						SpaceFillingCurveFunction.printBinary(z2);
						System.in.read();
					} catch (IOException e) {
					}
				} else {
					cnt++;
					if (cnt % 10000000L == 0L) {
						System.out.printf("%d\t correct\n", cnt);
					}
				}
			}
		}

		// test for 3D
		cnt = 0L;
		System.out.printf("%s\n", "Test SFCF for 3D");
		Collections.shuffle(nums);
		List<Long> nums2 = nums.subList(0, 500);
		for (Long x : nums2) {
			for (Long y : nums2) {
				for (Long z : nums2) {
					long[] l = {x, y, z};
					long z1 = SpaceFillingCurveFunction.zorderFunc(x, y, z);
					long z2 = SpaceFillingCurveFunction.zorderFunc(l);
					if (z1 != z2) {
						try {
							System.out.printf("%d\t%d\n", x, y);
							SpaceFillingCurveFunction.printBinary(z1);
							SpaceFillingCurveFunction.printBinary(z2);
							System.in.read();
						} catch (IOException e) {
						}
					} else {
						cnt++;
						if (cnt % 10000000L == 0L) {
							System.out.printf("%d\t correct\n", cnt);
						}
					}
				}
			}
		}

		// test for 4D
		cnt = 0L;
		System.out.printf("%s\n", "Test SFCF for 4D");
		Collections.shuffle(nums);
		List<Long> nums3 = nums.subList(0, 100);
		for (Long x : nums3) {
			for (Long y : nums3) {
				for (Long z : nums3) {
					for (Long w : nums3) {
						long[] l = {x, y, z, w};
						long z1 = SpaceFillingCurveFunction.zorderFunc(x, y, z, w);
						long z2 = SpaceFillingCurveFunction.zorderFunc(l);
						if (z1 != z2) {
							try {
								System.out.printf("%d\t%d\n", x, y);
								SpaceFillingCurveFunction.printBinary(z1);
								SpaceFillingCurveFunction.printBinary(z2);
								System.in.read();
							} catch (IOException e) {
							}
						} else {
							cnt++;
							if (cnt % 1000000L == 0L) {
								System.out.printf("%d\t correct\n", cnt);
								System.out.printf("%d\t%d\n", x, y);
								SpaceFillingCurveFunction.printBinary(z1);
								SpaceFillingCurveFunction.printBinary(z2);
							}
						}
					}
				}
			}
		}
	}
}
