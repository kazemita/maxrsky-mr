/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spatialindex.sfcf;

/**
 *
 * @author PowerWBH
 */
public class SFCFSceneMapper {

	public double[] lowerBounds;
	public double[] upperBounds;
	public int[] numCells;
	public int dimension;

	public SFCFSceneMapper(int dim) {
		lowerBounds = new double[dim];
		upperBounds = new double[dim];
		numCells = new int[dim];
		dimension = dim;
	}

	public void setParams(double[] lb, double[] ub, int nc[]) {
		System.arraycopy(lb, 0, lowerBounds, 0, dimension);
		System.arraycopy(ub, 0, upperBounds, 0, dimension);
		System.arraycopy(nc, 0, numCells, 0, dimension);
	}

	private long findCellNumber(double value, int idx) {
		double cellLength = (upperBounds[idx] - lowerBounds[idx]) / numCells[idx];
		double cellIdx = (value - lowerBounds[idx]) / cellLength;

		return ((Double) cellIdx).longValue();
	}

	public long getZOrderValue(double[] pos) throws Exception {
		if (pos.length != dimension) {
			throw new Exception("unmatched exception");
		}

		long[] num = new long[dimension];

		for (int i = 0; i < dimension; i++) {
			long cellIdx = findCellNumber(pos[i], i);
			if (cellIdx < 0 || cellIdx >= numCells[i]) {
				throw new Exception("exceed boundary");
			}

			num[i] = cellIdx;
		}
		
		return SpaceFillingCurveFunction.zorderFuncOpt(num);
	}
}
