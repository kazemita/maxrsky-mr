package spatialindex.rtree;

import java.util.ArrayList;
import java.util.HashMap;

public class RTreePack {
	ArrayList<RTree> trees = null;
	ArrayList<HashMap<Long, Long>> idMapper = null;
	int dimension;

	public RTreePack(ArrayList<String> rtreeDataPath, int dim) {
		dimension = dim;
		trees = new ArrayList<>();
		idMapper = new ArrayList<>();
		for (int i = 0; i < rtreeDataPath.size(); i++) {
			RTree rtree = new RTree(dimension);
			rtree.construct(rtreeDataPath.get(i));
			trees.add(rtree);

			int idx = rtreeDataPath.get(i).lastIndexOf("/");
			String mapFilename = rtreeDataPath.get(i).substring(0, idx)
					+ "/rmap";
			
			HashMap<Long, Long> mapper = new HashMap<>();
			readMap(mapFilename, mapper);
			idMapper.add(mapper);
		}
	}

	private void readMap(String path, HashMap<Long, Long> mapper) {
		try {
			FileSystemManager fsReader = FileSystemSelector.getFileSystem();
			fsReader.mapInputFile(path, 0, 1L << 55L);
			long size = fsReader.size();
			
			for (int i = 0; i < size / 16; i++) {
				mapper.put(fsReader.getLong(), fsReader.getLong());
			}
			
			fsReader.unmapInputFile();
		} catch (Exception e) {
			SystemLogger.error(e);
		}

	}

	public QueryResult query(SpatialObject so) {
		QueryResult qr = new QueryResult();

		System.out.println("tree size: " + trees.size());
		for (int i = 0; i < trees.size(); i++) {
			System.out.println("enter tree " + (i + 1));
			if (trees.get(i).getMBR().intersect(so)) {
				System.out.println("true intersect, do query");
				QueryResult tmpQR = trees.get(i).query(so);
				System.out.println("done query");
				for (Long id : tmpQR.resultIDs) {
					qr.resultIDs.add(idMapper.get(i).get(id));
				}
				for (Box box : tmpQR.resultBoxes) {
					qr.resultBoxes.add(box);
				}
			}
		}
		System.out.println("done true query");
		
		return qr;
	}
}
