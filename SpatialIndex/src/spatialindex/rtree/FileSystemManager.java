package spatialindex.rtree;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.ByteBuffer;

public interface FileSystemManager {

	public void mapInputFile(String filename, long start, long size)
			throws FileNotFoundException, IOException;

	public void unmapInputFile() throws IOException;

	public long getLong() throws IOException;

	public float getFloat() throws IOException;

	public void mapOutputFile(String filename) throws IOException;

	public void unmapOutputFile() throws IOException;

	public void write(ByteBuffer bb) throws IOException;

	public long size() throws IOException;

	public boolean rename(String oldName, String newName);
	
	public boolean exists(String filename);

	public void delete(String filename);

	public long readAll(String filename, ByteBuffer bb)
			throws FileNotFoundException, IOException;

	public void readBlock(ByteBuffer bb, long position) throws IOException;

	public String getParentPath(String filename);

	public boolean mkdir(String dir);

	public String findFilenameContain(String subString, String directory)
			throws FileNotFoundException, IOException;
}
