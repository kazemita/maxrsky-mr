/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spatialindex.rtree;

/**
 *
 * @author PowerWBH
 */
public class Point extends SpatialObject implements Comparable<Point>{

	public float[] vals;

	public Point(int dim) {
		vals = new float[dim];
		for (int i = 0; i < dim; i++) {
			vals[i] = 0;
		}
	}

	public Point(float[] values) {
		vals = new float[values.length];
		System.arraycopy(values, 0, vals, 0, values.length);
	}
	
	public Point(Point p) {
		vals = new float[p.vals.length];
		System.arraycopy(p.vals, 0, vals, 0, p.vals.length);
	}

	@Override
	public int compareTo(Point o) {
		for (int i = 0; i < vals.length; i++) {
			if (vals[i] < o.vals[i]) {
				return -1;
			} else if (vals[i] > o.vals[i]) {
				return 1;
			}
		}

		return 0;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("(");
		for (float v : vals) {
			sb.append(v);
			sb.append(", ");
		}
		
		sb.deleteCharAt(sb.length() - 1);
		sb.deleteCharAt(sb.length() - 1);
		sb.append(")");
		return sb.toString();
	}

	@Override
	public boolean intersect(SpatialObject so) {
		if (so instanceof Point) {
			Point p = (Point)so;
			if (compareTo(p) == 0)
				return true;
			else return false;
		} else if (so instanceof Box) {
			Box b = (Box)so;
			return b.contain(this);
		}
		
		return false;		
	}

	public float distSq(Point point) {
		float sum = 0.0f;
		for (int i = 0; i < point.vals.length; i++) {
			sum += (point.vals[i] - vals[i]) * (point.vals[i] - vals[i]);
		}
		
		return sum;
	}
	
	public float distAbsSum(Point point) {
		float sum = 0.0f;
		for (int i = 0; i < point.vals.length; i++) {
			sum += Math.abs(point.vals[i] - vals[i]);
		}
		
		return sum;
	}
}

