package spatialindex.rtree;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileChannel.MapMode;

public class LocalFSManager implements FileSystemManager {

	private MappedByteBuffer mbb = null;
	private FileChannel fc = null;
	private FileInputStream fis = null;
	private FileOutputStream fos = null;

	@Override
	public void mapInputFile(String filename, long start, long size)
			throws FileNotFoundException, IOException {
		fis = new FileInputStream(filename);
		fc = fis.getChannel();
		mbb = fc.map(MapMode.READ_ONLY, start, Math.min(size, fc.size()));

	}

	public static void unmap(FileChannel fc, MappedByteBuffer bb)
			throws Exception {
		Class<?> fcClass = fc.getClass();
		java.lang.reflect.Method unmapMethod = fcClass.getDeclaredMethod(
				"unmap", new Class[] { java.nio.MappedByteBuffer.class });
		unmapMethod.setAccessible(true);
		unmapMethod.invoke(null, new Object[] { bb });
	}

	@Override
	public void unmapInputFile() throws IOException {
		// TODO Auto-generated method stub
		fc.close();
		try {
			unmap(fc, mbb);
		} catch (Exception ex) {
			throw new IOException(ex.getMessage());
		} finally {
			fis.close();
		}

	}

	@Override
	public long getLong() throws IOException {
		// TODO Auto-generated method stub
		return mbb.getLong();
	}

	@Override
	public float getFloat() throws IOException {
		// TODO Auto-generated method stub
		return mbb.getFloat();
	}

	@Override
	public void mapOutputFile(String filename) throws IOException {
		// TODO Auto-generated method stub
		fos = new FileOutputStream(filename);
		fc = fos.getChannel();
	}

	@Override
	public void write(ByteBuffer bb) throws IOException {
		// TODO Auto-generated method stub
		fc.write(bb);
	}

	@Override
	public void unmapOutputFile() throws IOException {
		// TODO Auto-generated method stub
		fc.close();
		fos.close();
	}

	@Override
	public boolean rename(String oldName, String newName) {
		File f1 = new File(oldName);
		File f2 = new File(newName);

		return f1.renameTo(f2);
	}

	@Override
	public void delete(String filename) {
		File file = new File(filename);
		if (file.isDirectory()) {

			// directory is empty, then delete it
			if (file.list().length == 0) {

				if (file.delete()) {
					SystemLogger.debug("Directory is deleted : "
							+ file.getAbsolutePath());
				} else {
					SystemLogger.debug("Directory cannot be deleted : "
							+ file.getAbsolutePath());
				}

			} else {

				// list all the directory contents
				String files[] = file.list();

				for (String temp : files) {
					// construct the file structure
					File fileDelete = new File(file, temp);

					// recursive delete
					delete(fileDelete.getAbsolutePath());
				}

				// check the directory again, if empty then delete it
				if (file.list().length == 0) {
					if (file.delete()) {
						SystemLogger.debug("Directory is deleted : "
								+ file.getAbsolutePath());
					} else {
						SystemLogger.debug("Directory cannot be deleted : "
								+ file.getAbsolutePath());
					}
				}
			}

		} else {
			// if file, then delete it
			if (file.delete()) {
				SystemLogger.debug("File is deleted : "
						+ file.getAbsolutePath());
			} else {
				SystemLogger.debug("File cannot be deleted : "
						+ file.getAbsolutePath());
			}
		}
	}

	@Override
	public long size() throws IOException {
		return fc.size();
	}

	@Override
	public long readAll(String filename, ByteBuffer bb)
			throws FileNotFoundException, IOException {
		fis = new FileInputStream(filename);
		fc = fis.getChannel();
		long res = fc.read(bb);
		fc.close();
		fis.close();

		return res;
	}

	@Override
	public void readBlock(ByteBuffer bb, long position) throws IOException {
		fc.position(position);
		fc.read(bb);
	}

	@Override
	public String getParentPath(String filename) {
		return new File(filename).getParent();
	}

	@Override
	public boolean mkdir(String dir) {
		File theDir = new File(dir);
		if (!theDir.exists()) {
			System.out.println("creating directory: " + dir);
			boolean result = theDir.mkdir();
			if (result) {
				System.out.println("DIR created");
				return true;
			} else {
				return false;
			}
		} else {
			return true;
		}
	}

	@Override
	public String findFilenameContain(String subString, String directory)
			throws FileNotFoundException, IOException {
		String[] files = new File(directory).list();
		if (subString.contains("\\")) {
			subString = subString.replace("\\", "/");
		}

		for (String file : files) {
			file = new File(directory + "/" + file).getAbsolutePath();
			if (file.contains("\\")) {
				file = file.replace("\\", "/");
			}
			if (file.indexOf(subString) == 0) {
				return file;
			}
		}
		return null;
	}

	@Override
	public boolean exists(String filename) {
		try {
			if (new File(filename).exists()) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			SystemLogger.error(e);
			return false;
		}
	}

}
