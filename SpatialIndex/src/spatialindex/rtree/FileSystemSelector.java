package spatialindex.rtree;

public class FileSystemSelector {
	private static int fsid = 0;

	public static final String HDFS_FILESYSTEM_TYPE = "hdfs";
	public static final String LOCAL_FILESYSTEM_TYPE = "local";

	public static FileSystemManager getFileSystem() {
		if (fsid == 0) {
			return new LocalFSManager();
		} else if (fsid == 1) {
			return new HDFSManager();
		} else {
			return null;
		}
	}

	public static void config(String fileSystemType) {
		if (fileSystemType == HDFS_FILESYSTEM_TYPE)
			fsid = 1;
		else if (fileSystemType == LOCAL_FILESYSTEM_TYPE) {
			fsid = 0;
		}

	}
}
