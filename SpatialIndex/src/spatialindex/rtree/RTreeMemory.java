/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spatialindex.rtree;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.hadoop.io.Text;

/**
 *
 * @author PowerWBH
 */
public class RTreeMemory {

	int dimension;
	int rootLevel;
	ArrayList<RTreeNode> leaves;
	RTreeNode root;
	ByteBuffer bb;

	boolean bCreated = false;

	public RTreeMemory() {

	}

	public void construct(String filename, int dim) {
		ArrayList<String> records = new ArrayList<>();
		try {
			BufferedReader br = new BufferedReader(new FileReader(filename));
			String line = null;
			while ((line = br.readLine()) != null) {
				records.add(line);
			}

			br.close();
		} catch (FileNotFoundException ex) {
			SystemLogger.error(ex);
		} catch (IOException ex) {
			SystemLogger.error(ex);
		}

		construct(records, dim);
	}

	private int evaluateTreeSize(int size) {
		int levelSize = size;
		int memSize = 0;

		memSize += size * (8 + 8 * dimension);
		levelSize = (int) Math.ceil((double) levelSize / RTreeNode.MAX_NUM_CHILDREN);

		while (levelSize > 1) {
			memSize += levelSize * (8 + 8 * dimension);
			memSize += levelSize * 8 * RTreeNode.MAX_NUM_CHILDREN;

			levelSize = (int) Math.ceil((double) levelSize / RTreeNode.MAX_NUM_CHILDREN);
		}

		SystemLogger.info("Memory usage: " + String.valueOf((float) memSize / 1024.0f / 1024.0f) + "MB");

		return memSize + (1 << 20);
	}

	public void construct(Iterable<String> data, int dim) {
		dimension = dim;
		leaves = new ArrayList<>();

		for (String str : data) {
			String[] fields = str.split(",");
			float[] lbs = new float[dim];
			float[] ubs = new float[dim];
			RTreeNode rtn = new RTreeNode();

			rtn.dimension = dim;
			rtn.dp = null;
			rtn.id = Long.valueOf(fields[0]);
			for (int i = 0; i < dim; i++) {
				lbs[i] = Float.valueOf(fields[1 + i * 2]);
				ubs[i] = Float.valueOf(fields[1 + i * 2 + 1]);
			}
			rtn.lowerBounds = lbs;
			rtn.upperBounds = ubs;
			rtn.mp = null;
			rtn.type = 0;

			leaves.add(rtn);
		}

		bb = ByteBuffer.allocateDirect(evaluateTreeSize(leaves.size()));
		bb.putInt(dimension);
		bb.putInt(leaves.size());
		for (RTreeNode rtn : leaves) {
			bb.putLong(rtn.id);
			for (int i = 0; i < dim; i++) {
				bb.putFloat(rtn.lowerBounds[i]);
				bb.putFloat(rtn.upperBounds[i]);
			}
		}

		try {
			construct(leaves, 0);
		} catch (Exception ex) {
			SystemLogger.error(ex);
		}

		bb.flip();
		SystemLogger.info("R-Tree construction finished. Tree depth: " + (rootLevel + 1));
	}
	
	public void construct4Hadoop(Iterable<Text> data, int dim) {
		dimension = dim;
		leaves = new ArrayList<>();

		for (Text str : data) {
			String[] fields = str.toString().split(",");
			float[] lbs = new float[dim];
			float[] ubs = new float[dim];
			RTreeNode rtn = new RTreeNode();

			rtn.dimension = dim;
			rtn.dp = null;
			rtn.id = Long.valueOf(fields[0]);
			for (int i = 0; i < dim; i++) {
				lbs[i] = Float.valueOf(fields[1 + i * 2]);
				ubs[i] = Float.valueOf(fields[1 + i * 2 + 1]);
			}
			rtn.lowerBounds = lbs;
			rtn.upperBounds = ubs;
			rtn.mp = null;
			rtn.type = 0;

			leaves.add(rtn);
		}

		bb = ByteBuffer.allocateDirect(evaluateTreeSize(leaves.size()));
		bb.putInt(dimension);
		bb.putInt(leaves.size());
		for (RTreeNode rtn : leaves) {
			bb.putLong(rtn.id);
			for (int i = 0; i < dim; i++) {
				bb.putFloat(rtn.lowerBounds[i]);
				bb.putFloat(rtn.upperBounds[i]);
			}
		}

		try {
			construct(leaves, 0);
		} catch (Exception ex) {
			SystemLogger.error(ex);
		}

		bb.flip();
		SystemLogger.info("R-Tree construction finished. Tree depth: " + (rootLevel + 1));
	}

	private void construct(ArrayList<RTreeNode> nodes, int level) throws Exception {
		long numOfObjsPerNode = RTreeNode.MAX_NUM_CHILDREN;
		int numOneDim = (int) (Math.ceil((Double) Math.pow((double) nodes.size() / numOfObjsPerNode, 1.0 / dimension)));
		int newLevel = level + 1;
		ArrayList<RTreeNode> rtns = null;

		if (nodes.size() < RTreeNode.MAX_NUM_CHILDREN) {
			bCreated = true;
			rootLevel = level;

			RTreeNode rtn = new RTreeNode();
			rtn.dimension = dimension;
			rtn.dp = rtn.new DiskPointer();
			rtn.dp.children = new long[nodes.size()];
			int i = 0;
			for (RTreeNode r : nodes) {
				rtn.dp.children[i++] = r.id;
			}

			rtns = new ArrayList<>();
			rtns.add(rtn);
			root = rtn;
		} else {
			Sorter s = new Sorter();
			rtns = s.makePartition(nodes, dimension, numOneDim);
		}

		HashMap<Long, Integer> idMap = new HashMap<>();
		for (int i = 0; i < nodes.size(); i++) {
			idMap.put(nodes.get(i).id, i);
		}

		for (int r = 0; r < rtns.size(); r++) {
			RTreeNode rtn = rtns.get(r);

			float[] lbs = new float[dimension];
			float[] ubs = new float[dimension];

			for (int i = 0; i < dimension; i++) {
				lbs[i] = Float.MAX_VALUE;
				ubs[i] = -Float.MAX_VALUE;
			}

			rtn.mp = rtn.new MemoryPointer();
			rtn.mp.children = new RTreeNode[rtn.dp.children.length];

			for (int i = 0; i < rtn.dp.children.length; i++) {
				RTreeNode b = nodes.get(idMap.get(rtn.dp.children[i]));
				if (b.id != rtn.dp.children[i]) {
					throw new Exception("id mismatched");
				}

				for (int j = 0; j < dimension; j++) {
					lbs[j] = Math.min(lbs[j], b.lowerBounds[j]);
					ubs[j] = Math.max(ubs[j], b.upperBounds[j]);
				}

				rtn.mp.children[i] = b;
			}

			rtn.lowerBounds = lbs;
			rtn.upperBounds = ubs;
			rtn.id = r;
			rtn.dimension = dimension;
		}

		bb.putInt(rtns.size());
		for (int r = 0; r < rtns.size(); r++) {
			RTreeNode rtn = rtns.get(r);
			bb.putLong(rtn.id);
			for (int i = 0; i < dimension; i++) {
				bb.putFloat(rtn.lowerBounds[i]);
				bb.putFloat(rtn.upperBounds[i]);
			}
		}

		for (int r = 0; r < rtns.size(); r++) {
			RTreeNode rtn = rtns.get(r);
			bb.putInt(rtn.dp.children.length);
			for (int i = 0; i < rtn.dp.children.length; i++) {
				bb.putLong(rtn.dp.children[i]);
			}
		}

		SystemLogger.info("tree level " + level + " is successfully built");

		if (rtns.size() > 1) {
			construct(rtns, newLevel);
		}
	}

	private void query(SpatialObject so, ArrayList<Long> results, ArrayList<Box> resultBoxes, RTreeNode rtn) {
		Box mbr = new Box(dimension);
		mbr.set(rtn.lowerBounds, rtn.upperBounds);

		if (!mbr.intersect(so)) {
			return;
		}

		if (rtn.mp == null) {
			results.add(rtn.id);
			resultBoxes.add(mbr);
			return;
		} else {
			for (RTreeNode children : rtn.mp.children) {
				query(so, results, resultBoxes, children);
			}
		}
	}

	public ArrayList<Long> query(SpatialObject so) {
		ArrayList<Long> results = new ArrayList<>();
		ArrayList<Box> resultBoxes = new ArrayList<>();

		query(so, results, resultBoxes, root);

		if (resultBoxes.size() != 1) {
			System.out.println("shit~!!!!!!!!!!!");
			try {
				System.in.read();
			} catch (IOException ex) {

			}
		}

		return results;
	}

	public ByteBuffer buffer() {
		return bb;
	}

	public void readFrom(ByteBuffer bb) {
		this.bb = bb;

		dimension = bb.getInt();
		int size = bb.getInt();
		leaves = new ArrayList<>(size);
		for (int i = 0; i < size; i++) {
			RTreeNode rtn = new RTreeNode();
			rtn.dimension = dimension;
			rtn.dp = null;
			rtn.id = bb.getLong();
			rtn.lowerBounds = new float[dimension];
			rtn.upperBounds = new float[dimension];

			for (int j = 0; j < dimension; j++) {
				rtn.lowerBounds[j] = bb.getFloat();
				rtn.upperBounds[j] = bb.getFloat();
			}

			rtn.mp = null;
			rtn.type = 0;

			leaves.add(rtn);
		}

		HashMap<Long, Integer> idMap = new HashMap<>();
		ArrayList<RTreeNode> prevLevel = leaves;
		for (int i = 0; i < leaves.size(); i++) {
			idMap.put(leaves.get(i).id, i);
		}
		
		rootLevel = 0;

		while (true) {
			try {
				size = bb.getInt();
				ArrayList<RTreeNode> rtns = new ArrayList<>(size);
				for (int i = 0; i < size; i++) {
					RTreeNode rtn = new RTreeNode();
					rtn.dimension = dimension;
					rtn.id = bb.getLong();
					rtn.lowerBounds = new float[dimension];
					rtn.upperBounds = new float[dimension];

					for (int j = 0; j < dimension; j++) {
						rtn.lowerBounds[j] = bb.getFloat();
						rtn.upperBounds[j] = bb.getFloat();
					}

					rtn.dp = rtn.new DiskPointer();
					rtn.mp = rtn.new MemoryPointer();
					rtn.type = 1;

					rtns.add(rtn);
				}

				for (int i = 0; i < rtns.size(); i++) {
					RTreeNode rtn = rtns.get(i);
					rtn.dp.children = new long[bb.getInt()];
					rtn.mp.children = new RTreeNode[rtn.dp.children.length];

					for (int j = 0; j < rtn.dp.children.length; j++) {
						rtn.dp.children[j] = bb.getLong();
						rtn.mp.children[j] = prevLevel.get(idMap.get(rtn.dp.children[j]));
					}
				}

				if (rtns.size() > 1) {
					prevLevel = rtns;
					for (int i = 0; i < size; i++) {
						idMap.put(rtns.get(i).id, i);
					}
					rootLevel++;
				} else {
					root = rtns.get(0);
					bCreated = true;
					break;
				}

			} catch (BufferUnderflowException ex) {
				SystemLogger.error(ex);
			}
		}
	}
}
