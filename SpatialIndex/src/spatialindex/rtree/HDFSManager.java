package spatialindex.rtree;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URI;
import java.nio.ByteBuffer;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

public class HDFSManager implements FileSystemManager {
	private static FileSystem fs = null;

	private String filename = null;
	private FSDataInputStream fsdis = null;
	private FSDataOutputStream fsdos = null;

	public static void init(URI uri, Configuration conf) throws IOException {
		fs = FileSystem.get(uri, conf);
	}

	public static void init(Configuration conf) throws IOException {
		fs = FileSystem.get(conf);
	}

	public static void init(FileSystem fileSystem) {
		fs = fileSystem;
	}

	@Override
	public void mapInputFile(String filename, long start, long size)
			throws FileNotFoundException, IOException {
		int trytimes = 40;
		while (trytimes > 0) {
			try {
				fsdis = fs.open(new Path(filename));
				if (fsdis == null) {
					throw new Exception();
				}
				break;
			} catch (Exception e) {
				try {
					Thread.sleep(500);
					trytimes--;
				} catch (InterruptedException e1) {
				}
			}
		}

		if (trytimes > 0) {
			fsdis.seek(start);
			this.filename = filename;
		} else {
			throw new FileNotFoundException(filename + " no found");
		}
	}

	@Override
	public void unmapInputFile() throws IOException {
		fsdis.close();
	}

	@Override
	public long getLong() throws IOException {
		return fsdis.readLong();
	}

	@Override
	public float getFloat() throws IOException {
		return fsdis.readFloat();
	}

	@Override
	public void mapOutputFile(String filename) throws IOException {
		fsdos = fs.create(new Path(filename));
		this.filename = filename;
	}

	@Override
	public void write(ByteBuffer bb) throws IOException {
		byte[] bary = new byte[bb.remaining()];
		bb.get(bary);
		fsdos.write(bary);
	}

	@Override
	public void unmapOutputFile() throws IOException {
		fsdos.close();

	}

	@Override
	public boolean rename(String oldName, String newName) {
		try {
			int trytimes = 20;
			while (!fs.rename(new Path(oldName), new Path(newName))
					&& trytimes > 0) {
				Thread.sleep(500);
				trytimes++;
			}
			if (trytimes > 0)
				return true;
			else {
				throw new IOException("cannot rename file:" + oldName + "; "
						+ newName);
			}
		} catch (IOException ex) {
			SystemLogger.error(ex);
			return false;
		} catch (InterruptedException ex) {
			SystemLogger.error(ex);
			return false;
		}
	}

	@Override
	public void delete(String filename) {
		try {
			if (fs.delete(new Path(filename), true)) {
				SystemLogger.debug("file " + filename + " is removed.");
			}
		} catch (IOException ex) {
			SystemLogger.error(ex);
		}
	}

	@Override
	public long size() throws IOException {
		return fs.getFileStatus(new Path(filename)).getLen();
	}

	@Override
	public boolean exists(String filename) {
		try {
			if (fs.exists(new Path(filename))) {
				return true;
			} else
				return false;
		} catch (IOException ex) {
			SystemLogger.error(ex);
			return false;
		}

	}

	public static FileSystem getFS() {
		return fs;
	}

	@Override
	public long readAll(String filename, ByteBuffer bb)
			throws FileNotFoundException, IOException {
		mapInputFile(filename, 0, 0);

		byte[] tmpBuffer = new byte[(int) size()];
		fsdis.readFully(tmpBuffer);
		fsdis.close();

		bb.put(tmpBuffer);
		// bb.flip();

		return tmpBuffer.length;
	}

	@Override
	public void readBlock(ByteBuffer bb, long position) throws IOException {
		byte[] tmpBuf = new byte[bb.remaining()];
		int off = 0;
		//SystemLogger.debug("read position: " + position + " try size: " + tmpBuf.length);
		try {
			fsdis.seek(position);

			while (off < tmpBuf.length) {
				int readLen = fsdis.read(tmpBuf, off, tmpBuf.length - off);
				if (readLen > 0)
					off += readLen;
				else if (readLen <= 0)
					break;
			}
		} catch (IOException ex) {
			throw ex;
		} catch (Exception e) {
			SystemLogger.info("error happens off: " + off + " len: "
					+ tmpBuf.length);
			SystemLogger.error(e);
		}

		//SystemLogger.debug("read position: " + position + " get size: " + off);
		bb.put(tmpBuf);
	}

	@Override
	public String getParentPath(String filename) {
		return new Path(filename).getParent().toString();
	}

	@Override
	public boolean mkdir(String dir) {
		Path dirPath = new Path(dir);
		try {
			if (!fs.exists(dirPath) || !fs.isDirectory(dirPath)) {
				SystemLogger.info("creating directory: " + dir);
				boolean result = fs.mkdirs(dirPath);
				if (result) {
					SystemLogger.info("DIR created");
					return true;
				} else {
					return false;
				}
			} else {
				return true;
			}
		} catch (IOException ex) {
			SystemLogger.error(ex);
			return false;
		}
	}

	@Override
	public String findFilenameContain(String subString, String directory)
			throws FileNotFoundException, IOException {
		FileStatus[] files = fs.listStatus(new Path(directory));
		for (FileStatus file : files) {
			if (file.getPath().toString().indexOf(subString) == 0) {
				return file.getPath().toString();
			}
		}

		return null;
	}

}
