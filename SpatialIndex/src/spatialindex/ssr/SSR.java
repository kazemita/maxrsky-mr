package spatialindex.ssr;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

import spatialindex.rtree.Box;
import spatialindex.rtree.Point;

public class SSR {
	private DSLObject obj = null;
	private SSRTree ssrTree = null;
	private int dimension = 0;

	public SSR(DSLObject dslObj, Box boundary, int dimension) {
		obj = dslObj;
		ssrTree = new SSRTree(obj, boundary, dimension);
		//ssrTree.treeCheck();
		this.dimension = dimension;
	}

	public ArrayList<Point> intersect(SSR ssr) {
		ArrayList<Box> boxes = ssr.ssrTree.getAllBoxes();
		ArrayList<Point> intPoints = new ArrayList<>();
		ArrayList<Box> srcBoxes = new ArrayList<>();
		Queue<SSRTreeNode> bfsQueue = new LinkedList<>();

		if (ssrTree.rootNode.mbr.intersect(ssr.ssrTree.rootNode.mbr) == false) {
			return intPoints;
		}

		bfsQueue.offer(ssrTree.rootNode);

		while (bfsQueue.isEmpty() == false) {
			SSRTreeNode node = bfsQueue.poll();

			if (node.children == null) {
				for (Box box : boxes) {
					if (node.mbr.intersect(box)) {
						srcBoxes.add(node.mbr);
						break;
					}
				}
			} else {
				for (SSRTreeNode childrenNode : node.children) {
					boolean bFlag = false;
					for (Box box : boxes) {
						if (childrenNode.mbr.intersect(box)) {
							bFlag = true;
							break;
						}
					}
					if (bFlag) {
						bfsQueue.offer(childrenNode);
					}
				}
			}
		}

		for (Box b1 : srcBoxes) {
			for (Box b2 : boxes) {
				b1.getIntersectPoint(intPoints, b2);
			}
		}

		if (intPoints.size() == 0) {
			return intPoints;
		}

		Collections.sort(intPoints);
		ArrayList<Point> intPoints2 = new ArrayList<>(intPoints.size());
		intPoints2.add(intPoints.get(0));

		for (int i = 1; i < intPoints.size(); i++) {
			if (intPoints.get(i).distAbsSum(
					intPoints2.get(intPoints2.size() - 1)) > Float.MIN_VALUE
					* dimension) {
				intPoints2.add(intPoints.get(i));
			}
		}

		return intPoints2;
	}

	public ArrayList<Point> intersectNaive(SSR ssr) {
		ArrayList<Box> boxes1 = ssr.ssrTree.getAllBoxes();
		ArrayList<Box> boxes2 = ssrTree.getAllBoxes();
		ArrayList<Point> intPoints = new ArrayList<>();

		for (Box b1 : boxes1) {
			for (Box b2 : boxes2) {
				if (b1.intersect(b2)) {
					b1.getIntersectPoint(intPoints, b2);
				}
			}
		}

		if (intPoints.size() == 0) {
			return intPoints;
		}

		Collections.sort(intPoints);
		ArrayList<Point> intPoints2 = new ArrayList<>(intPoints.size());
		intPoints2.add(intPoints.get(0));

		for (int i = 1; i < intPoints.size(); i++) {
			if (intPoints.get(i).distAbsSum(
					intPoints2.get(intPoints2.size() - 1)) > Float.MIN_VALUE
					* dimension) {
				intPoints2.add(intPoints.get(i));
			}
		}

		return intPoints2;
	}

	public ArrayList<Point> intersectImproved(SSR ssr) {
		ArrayList<Point> intPoints = new ArrayList<>();
		Queue<SSRTreeNode> bfsQueue1 = new LinkedList<>();
		Queue<SSRTreeNode> bfsQueue2 = new LinkedList<>();
		Set<SSRTreeNode> intersectedNode1 = new HashSet<>();
		Set<SSRTreeNode> intersectedNode2 = new HashSet<>();
		boolean bLastLevel1 = false, bLastLevel2 = false;

		bfsQueue1.offer(ssrTree.rootNode);
		bfsQueue2.offer(ssr.ssrTree.rootNode);

		while (true) {
			intersectedNode1.clear();
			intersectedNode2.clear();
			// find all nodes in 1 that may be intersected further
			for (SSRTreeNode n1 : bfsQueue1) {
				for (SSRTreeNode n2 : bfsQueue2) {
					if (n1.mbr.intersect(n2.mbr)) {
						intersectedNode1.add(n1);
						intersectedNode2.add(n2);
					}
				}
			}

			if (bLastLevel1 == false) {
				bfsQueue1.clear();
				int numNewAdded = 0;
				// add children node
				for (SSRTreeNode n1 : intersectedNode1) {
					if (n1.children != null) {
						for (SSRTreeNode nc : n1.children) {
							bfsQueue1.offer(nc);
							numNewAdded++;
						}
					} else
						bfsQueue1.offer(n1);
				}
				// last level
				if (numNewAdded == 0) {
					bLastLevel1 = true;
				}
			}

			if (bLastLevel2 == false) {
				bfsQueue2.clear();
				int numNewAdded = 0;
				// add children node
				for (SSRTreeNode n2 : intersectedNode2) {
					if (n2.children != null) {
						for (SSRTreeNode nc : n2.children) {
							bfsQueue2.offer(nc);
							numNewAdded++;
						}
					} else
						bfsQueue2.offer(n2);
				}
				// last level
				if (numNewAdded == 0) {
					bLastLevel2 = true;
				}
			}

			if (bLastLevel1 && bLastLevel2)
				break;
		}
		// compute intersection point
		for (SSRTreeNode n1 : bfsQueue1) {
			for (SSRTreeNode n2 : bfsQueue2) {
				n1.mbr.getIntersectPoint(intPoints, n2.mbr);
			}
		}
		// no intersection
		if (intPoints.size() == 0) {
			return intPoints;
		}

		Collections.sort(intPoints);
		ArrayList<Point> intPoints2 = new ArrayList<>(intPoints.size());
		intPoints2.add(intPoints.get(0));

		for (int i = 1; i < intPoints.size(); i++) {
			if (intPoints.get(i).distAbsSum(
					intPoints2.get(intPoints2.size() - 1)) > Float.MIN_VALUE
					* dimension) {
				intPoints2.add(intPoints.get(i));
			}
		}

		return intPoints2;
	}

	public ArrayList<Point> intersect(Box mbr) {
		ArrayList<Point> intPoints = new ArrayList<>();
		ArrayList<Box> srcBoxes = new ArrayList<>();
		Queue<SSRTreeNode> bfsQueue = new LinkedList<>();

		if (ssrTree.rootNode.mbr.intersect(mbr) == false) {
			return intPoints;
		}

		bfsQueue.offer(ssrTree.rootNode);

		while (bfsQueue.isEmpty() == false) {
			SSRTreeNode node = bfsQueue.poll();

			if (node.children == null) {
				srcBoxes.add(node.mbr);
			} else {
				for (SSRTreeNode childrenNode : node.children) {
					if (childrenNode.mbr.intersect(mbr)) {
						bfsQueue.add(childrenNode);
					}
				}
			}
		}

		for (Box b1 : srcBoxes) {
			b1.getIntersectPoint(intPoints, mbr);
		}

		if (intPoints.size() == 0) {
			return intPoints;
		}

		Collections.sort(intPoints);
		ArrayList<Point> intPoints2 = new ArrayList<>(intPoints.size());
		intPoints2.add(intPoints.get(0));

		for (int i = 1; i < intPoints.size(); i++) {
			if (intPoints.get(i).distAbsSum(
					intPoints2.get(intPoints2.size() - 1)) > Float.MIN_VALUE
					* dimension) {
				intPoints2.add(intPoints.get(i));
			}
		}

		return intPoints2;
	}

	public SSRTree getSSRTree() {
		return ssrTree;
	}
}
