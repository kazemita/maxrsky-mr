package spatialindex.ssr;

import java.util.ArrayList;

import spatialindex.rtree.Box;

public class SSRTreeNode {
	public Box mbr = null;
	public ArrayList<SSRTreeNode> children = null;
	public int level = 0;
}
