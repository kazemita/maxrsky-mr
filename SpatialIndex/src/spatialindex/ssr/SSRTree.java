package spatialindex.ssr;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import spatialindex.rtree.Box;
import spatialindex.rtree.Point;

public class SSRTree {

	public SSRTreeNode rootNode = null;
	int dim;

	class Pair {
		public int idx;
		public float v;
	}

	public SSRTree(DSLObject dslObj, Box boundary, int dimension) {
		ArrayList<Pair> d0points = new ArrayList<>();
		ArrayList<Point> dnpoints = new ArrayList<>();
		SSRTreeNode root = new SSRTreeNode();
		root = new SSRTreeNode();
		root.children = new ArrayList<>();
		root.mbr = new Box(dimension);
		root.level = 1;

		for (int d = 0; d < dimension; d++) {
			root.mbr.lowerBounds[d] = 0.0f;
			root.mbr.upperBounds[d] = boundary.upperBounds[d]
					- dslObj.origPoint.vals[d];
		}

		for (int i = 0; i < dslObj.points.length; i++) {
			Pair p = new Pair();
			p.idx = i;
			p.v = dslObj.points[i].vals[0];

			d0points.add(p);
		}

		Collections.sort(d0points, new Comparator<Pair>() {

			@Override
			public int compare(Pair o1, Pair o2) {
				if (o1.v - o2.v < 0.0f) {
					return -1;
				} else if (o1.v > o2.v) {
					return 1;
				} else {
					return 0;
				}
			}

		});

		for (int i = 0; i < d0points.size(); i++) {
			dnpoints.add(dslObj.points[d0points.get(i).idx]);
		}
		// construct relative space split box;
		construct(dnpoints, dimension, root);
		dim = dimension;
		// un-fold *************************************
		//
		rootNode = new SSRTreeNode();
		rootNode.children = new ArrayList<>();
		rootNode.mbr = new Box(dimension);

		SSRTreeNode tmpTreeNode = new SSRTreeNode();
		ArrayList<SSRTreeNode> tmpRoots = new ArrayList<>(1 << dimension);
		Point inv = new Point(dim);
		Point noOffset = new Point(dim);
		Point hasOffset = new Point(dim);
		for (int d = 0; d < dim; d++) {
			inv.vals[d] = 1.0f;
			hasOffset.vals[d] = 1.0f;
			noOffset.vals[d] = 0.0f;
		}
		copyTree(root, tmpTreeNode, dslObj.origPoint, inv, noOffset);
		rootNode.children.add(tmpTreeNode);

		for (int i = 0; i < dimension; i++) {
			Point tmpInv = new Point(inv);
			tmpInv.vals[i] = -1.0f;
			tmpRoots.clear();

			for (SSRTreeNode tmpRoot : rootNode.children) {
				tmpTreeNode = new SSRTreeNode();
				copyTree(tmpRoot, tmpTreeNode, dslObj.origPoint, tmpInv,
						hasOffset);
				tmpRoots.add(tmpTreeNode);
			}

			for (SSRTreeNode tmpRoot : tmpRoots) {
				rootNode.children.add(tmpRoot);
			}
		}
		// compute mbr for all
		rootNode.mbr.setMinMax();
		for (SSRTreeNode tmpRoot : rootNode.children) {
			for (int i = 0; i < dimension; i++) {
				rootNode.mbr.lowerBounds[i] = Math
						.min(rootNode.mbr.lowerBounds[i],
								tmpRoot.mbr.lowerBounds[i]);
				rootNode.mbr.upperBounds[i] = Math
						.max(rootNode.mbr.upperBounds[i],
								tmpRoot.mbr.upperBounds[i]);
			}
		}
		rootNode.level = 0;
	}

	private void construct(ArrayList<Point> points, int dimension,
			SSRTreeNode root) {
		int midIdx = points.size() >> 1;
		ArrayList<Box> boxes = new ArrayList<>();
		Box remainSpace = root.mbr.copy();

		for (int d = 0; d < dimension; d++) {
			Box tmpBox = remainSpace.copy(), tmpBoxRemain = remainSpace.copy();
			tmpBox.upperBounds[d] = points.get(midIdx).vals[d];
			tmpBoxRemain.lowerBounds[d] = points.get(midIdx).vals[d];

			boxes.add(tmpBox);
			remainSpace = tmpBoxRemain;
		}

		for (int i = 0; i < boxes.size(); i++) {
			ArrayList<Point> childrenPoints = new ArrayList<>();
			for (int j = 0; j < points.size(); j++) {
				if (boxes.get(i).contain(points.get(j)) && j != midIdx) {
					childrenPoints.add(points.get(j));
				}
			}

			SSRTreeNode node = new SSRTreeNode();
			node.mbr = boxes.get(i).copy();
			node.level = root.level + 1;

			if (childrenPoints.size() > 0) {
				node.children = new ArrayList<>();
				construct(childrenPoints, dimension, node);
			} else {
				node.children = null;
			}

			root.children.add(node);
		}
	}

	private void getAllBoxes(ArrayList<Box> boxes, SSRTreeNode root) {
		if (root.children == null) {
			boxes.add(root.mbr);
		} else {
			for (SSRTreeNode node : root.children) {
				getAllBoxes(boxes, node);
			}
		}
	}

	private void copyTree(SSRTreeNode srcRoot, SSRTreeNode distNode,
			Point offset, Point inv, Point hasOffset) {
		// copy mbr
		distNode.mbr = new Box(dim);
		distNode.level = srcRoot.level;
		for (int d = 0; d < dim; d++) {
			float lb = (srcRoot.mbr.lowerBounds[d] - offset.vals[d]
					* hasOffset.vals[d])
					* inv.vals[d] + offset.vals[d];
			float ub = (srcRoot.mbr.upperBounds[d] - offset.vals[d]
					* hasOffset.vals[d])
					* inv.vals[d] + offset.vals[d];

			distNode.mbr.lowerBounds[d] = Math.min(lb, ub);
			distNode.mbr.upperBounds[d] = Math.max(lb, ub);
		}

		if (srcRoot.children == null) {
			distNode.children = null;
			return;
		}

		distNode.children = new ArrayList<>();
		for (int i = 0; i < srcRoot.children.size(); i++) {
			SSRTreeNode tempNode = new SSRTreeNode();

			copyTree(srcRoot.children.get(i), tempNode, offset, inv, hasOffset);
			distNode.children.add(tempNode);
		}
	}

	public ArrayList<Box> getAllBoxes() {
		ArrayList<Box> boxes = new ArrayList<>();
		getAllBoxes(boxes, rootNode);

		return boxes;
	}

	@Override
	public String toString() {
		ArrayList<Box> boxes = new ArrayList<>();
		StringBuilder sb = new StringBuilder();

		getAllBoxes(boxes, rootNode);
		for (Box b : boxes) {
			sb.append(b.toString());
			sb.append("||");
		}

		return sb.toString();
	}

	private boolean treeCheck(SSRTreeNode root) {
		if (!root.mbr.boxCheck()) {
			System.out.printf("mbr error %s\n", root.mbr.toString());
			return false;
		}
		
		if (root.children == null)
			return true;

		for (SSRTreeNode node : root.children) {
			if (!root.mbr.contain(new Point(node.mbr.lowerBounds))
					|| !root.mbr.contain(new Point(node.mbr.upperBounds))) {
				System.out.printf("tree error at node %s with parent %s\n",
						node.mbr.toString(), root.mbr.toString());
				return false;
			}

			if (treeCheck(node) == false)
				return false;
		}

		return true;
	}

	public boolean treeCheck() {
		return treeCheck(rootNode);
	}
}
