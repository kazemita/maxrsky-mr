/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spatialindex;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.util.ArrayList;
import mr.spatialindex.MapReduceUtility;

import spatialindex.rtree.Box;
import spatialindex.rtree.Point;
import spatialindex.rtree.QueryResult;
import spatialindex.rtree.RTree;
import spatialindex.ssr.DSLObject;
import spatialindex.ssr.SSR;

/**
 * 
 * @author PowerWBH
 */
public class SpatialIndex {

	public static class Pair {

		public double x, y;

		public double[] toArray() {
			double[] a = { x, y };
			return a;
		}
	}

	/**
	 * @param args
	 *            the command line arguments
	 */
	// public static void main(String[] args) {
	// TreeMap<Long, Pair> coordSorted = new TreeMap<>();
	// ArrayList<Pair> coordinates = new ArrayList<>();
	// SFCFSceneMapper sfcfsm = new SFCFSceneMapper(2);
	// double[] lb = {0.0, 0.0};
	// double[] ub = {512.0, 512.0};
	// int[] nc = {16, 16};
	// double mx = 512.0, step = 32.0;
	//
	// sfcfsm.setParams(lb, ub, nc);
	//
	// for (double y = step * 0.5; y < mx; y += step) {
	// for (double x = step * 0.5; x < mx; x += step) {
	// Pair p = new Pair();
	// p.x = x;
	// p.y = y;
	// coordinates.add(p);
	// }
	// }
	//
	// for (Pair p : coordinates) {
	// try {
	// coordSorted.put(sfcfsm.getZOrderValue(p.toArray()), p);
	// } catch (Exception e) {
	// System.err.println(e.getMessage());
	// }
	// }
	//
	// try {
	// BufferedWriter bw = new BufferedWriter(new
	// FileWriter("c:\\tmp\\dat.dd"));
	// for (Map.Entry<Long, Pair> kvp : coordSorted.entrySet()) {
	// bw.write(String.format("%.2f\t%.2f\n", kvp.getValue().x,
	// kvp.getValue().y));
	// }
	//
	// bw.close();
	// } catch (IOException ioe) {
	// System.err.println(ioe.getMessage());
	// }
	//
	// }
	// Sorter s1 = new Sorter();
	// s.GenerateData();
	// s.sort("c:\\tmp\\dd\\d", 0, 1000);
	// s.PrintDatafile("c:\\tmp\\dd\\d-p-111");
	// s.makePartition("c:\\tmp\\dd\\d", 1000, 10000000);
	// s1.sort("c:\\tmp\\dd\\d1", 0, 10000000);
	// System.out.println("sort finished, partition starts");
	// long size = s1.makePartition("c:\\tmp\\dd\\d1-sort", 10, 10000000);
	// System.out.println(size);
	// s1.sort("c:\\tmp\\dd\\d2", 0, 10000000);
	// s1.join("c:\\tmp\\dd\\d1-sort-p-1", "c:\\tmp\\dd\\d2-sort", 1000001,
	// 10000000);
	// s1.PrintDatafile("c:\\tmp\\dd\\d1-sort-p-1-join");

	public static void testRTree(String filename, String testFilename, int dim,
			int numRecords) {
		try {
			RTree rt = new RTree(dim);
			long fileSize = new File(filename + "-0").length();
			long nr = fileSize / (8 * (dim + 1));
			rt.construct(filename, nr);
			DataInputStream dis = new DataInputStream(new FileInputStream(
					testFilename));
			while (true) {
				try {
					Point p = new Point(dim);
					for (int i = 0; i < dim; i++) {
						p.vals[i] = dis.readFloat();
					}

					QueryResult result = rt.query(p);
					if (result.resultIDs.size() == 0) {
						System.err.println("error test " + p.toString());

					} else {
						System.out.printf("totally %d result(s)\n",
								result.resultIDs.size());
						for (Box b : result.resultBoxes) {
							if (b.contain(p) == false) {
								System.err.println("error test " + p.toString()
										+ " " + b.toString());
							}
						}
					}

					// if (c % 1 == 0) {
					// System.out.println(c);
					// }
				} catch (EOFException e) {
					break;
				}
			}

			dis.close();
		} catch (Exception e) {

		}
	}

	private static void testSplit() {
		try {
			int dim = 4;
			BufferedReader br = new BufferedReader(new FileReader("f:\\da"));
			ArrayList<DSLObject> objs = new ArrayList<>();
			new ArrayList<>();
			Box mbr = new Box(dim);
			Box queryMbr = new Box(dim);
			String line = null;

			for (int d = 0; d < dim; d++) {
				mbr.lowerBounds[d] = 0.0f;
				mbr.upperBounds[d] = 0.0f;
			}

			while ((line = br.readLine()) != null) {
				DSLObject dslObj = MapReduceUtility.getPointsFromString(line,
						dim);
				Box localMbr = MapReduceUtility.getBoxFromDSL(dslObj, dim);
				objs.add(dslObj);

				for (int d = 0; d < dim; d++) {
					mbr.lowerBounds[d] = Math.min(mbr.lowerBounds[d],
							localMbr.lowerBounds[d]);
					mbr.upperBounds[d] = Math.max(mbr.upperBounds[d],
							localMbr.upperBounds[d]);
				}
			}

			br.close();

			for (int d = 0; d < dim; d++) {
				mbr.upperBounds[d] += 0.1f;
				mbr.lowerBounds[d] -= 0.1f;
				queryMbr.upperBounds[d] =  mbr.upperBounds[d] * 0.2f;
				queryMbr.lowerBounds[d] =  mbr.lowerBounds[d] * 0.2f;
			}

			System.out.printf("global mbr is %s", mbr.toString());

			long t1 = System.nanoTime();
			int num = 0;
			int num1 = 0;
			int num2 = 0;
			int num3 = 0;
			for (int i = 0; i < objs.size(); i++) {
				SSR ssr = new SSR(objs.get(i), mbr, dim);
				ssr.intersect(queryMbr).size();
					
					//System.out.printf("%d\t%d\n", s1, s);
					//System.in.read();
				//}
				
				//if (s > 0) {
					//System.out.printf("int: %d\n", s);
				//	num2 += s;
				//	num3 += 1;
				//}

				//num += ssr.getSSRTree().getAllBoxes().size();
				//num1 += objs.get(i).points.length;
				

				

				//if (i % 1000 == 0) {
				//	System.out.println(i);
				//}
			}
			long t2 = System.nanoTime();
			double te = (t2 - t1) / 1000000.0;

			System.out.printf("total time: %.4fms\n" + "average time: %.4f\n"
					+ "average number of boxes: %.4f\n"
					+ "average number of dsl points: %.4f\n"
					+ "average number of int point: %.4f\n", te,
					te / objs.size(), (float) num / objs.size(), (float) num1
							/ objs.size(), (float) num2 / num3);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		// try {
		// BufferedReader br = new BufferedReader(new
		// FileReader("c:\\tmp\\rect\\dsl"));
		// String line = null;
		// int dim = 4;
		// while ((line = br.readLine()) != null) {
		// Point[] points = MapReduceUtility.getPointsFromString(line, dim);
		// ArrayList<Box> mbrs = MapReduceUtility.getBoxesFromDSL(points, 3,
		// dim);
		// for (Box b : mbrs) {
		// System.out.println(b.toString());
		// }
		// System.in.read();
		// }
		// br.close();
		// } catch (Exception e) {
		// e.printStackTrace(System.err);
		// }

		// RectGenerator.GenerateRectsText("c:\\tmp\\rect-grid-4d");
		// try {
		// FileSystemSelector.config(FileSystemSelector.HDFS_FILESYSTEM_TYPE);
		// HDFSManager.init(URI.create("s3://dbprj/"), new Configuration());
		// //HDFSManager.init(new Configuration());
		// } catch (IOException e) {
		// e.printStackTrace();
		// }
		// testRTree("c:/tmp/rect/rdata", "c:/tmp/rect/rect-grid-p", 2, 1 <<
		// 22);
		testSplit();

		// RTree rt = new RTree(4);
		// rt.construct("s3://dbprj/rect-grid4d", 1 << 20);
		// System.out.println(rt.convertDataset("c:\\tmp\\rect\\rect"));
		// rt.construct("c:\\tmp\\rect\\rect-grid4d", 1 << 20);
		// rt.construct("c:\\tmp\\rect\\rect-grid4d");
		//

		// RectGenerator.GenerateRects("c:\\tmp\\rect\\rect-grid8d");
		// RectGenerator.GenerateRectsText("c:\\tmp\\rect\\rect-grid-2d");
		// RectGenerator.GenerateRects("rect-grid-2d");

		// RTreeMemory rtree= new RTreeMemory();
		//
		// //rtree.construct("C:\\tmp\\rect\\rect-grid-2d", 2);
		//
		// try {
		// //FileChannel fc = new
		// FileOutputStream("c:\\tmp\\rect\\rtree").getChannel();
		// FileChannel fc = new
		// FileInputStream("c:\\tmp\\rect\\rtree").getChannel();
		// ByteBuffer bb = ByteBuffer.allocateDirect((int)fc.size());
		// fc.read(bb);
		// fc.close();
		// bb.flip();
		// rtree.readFrom(bb);
		// }catch(Exception e) {
		//
		// }
		//
		//
		//
		// ArrayList<String> records = new ArrayList<>();
		// try {
		// BufferedReader br = new BufferedReader(new
		// FileReader("C:\\tmp\\rect\\rect-grid-2d-p"));
		// String line = null;
		// while ((line = br.readLine()) != null) {
		// records.add(line);
		// }
		//
		// br.close();
		// } catch (FileNotFoundException ex) {
		// SystemLogger.error(ex);
		// } catch (IOException ex) {
		// SystemLogger.error(ex);
		// }
		//
		// int cnt = 0;
		// for (String q : records) {
		// String[] fields = q.split(",");
		// Point p = new Point(2);
		// p.vals[0] = Float.valueOf(fields[0]);
		// p.vals[1] = Float.valueOf(fields[1]);
		// rtree.query(p);
		// cnt++;
		//
		// if (cnt % 1000 == 0) {
		// SystemLogger.info("[" + cnt + "]");
		// }
		// }
	}

}
