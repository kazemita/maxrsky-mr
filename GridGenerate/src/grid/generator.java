package grid;

public class generator {
	
	public static int offset = 10;
	public static int DIM = 6;
	public static int values[] = new int[6];
	
	public static void recursive_loop(int depth, int min, int max) {
		for (int i = min; i < max; i += offset) {
			if(depth < DIM - 1) {
				values[depth] = i;
				recursive_loop(depth + 1, min, max);
			} else {
				values[depth] = i;
//				System.out.println(Arrays.toString(values));
			}
		}
	}
	
	public static void main(String[] args) {
		
		long before = System.currentTimeMillis();
		
//		for(int i = 0; i < 200; i++) {
//			for(int j = 0; j < 200; j++) {
//				for(int k = 0; k < 200; k++) {
//					for(int l = 0; l < 200; l++) {
//						for(int m = 0; m < 200; m++) {
//							for (int n = 0; n < 200; n++) {
//								
//							}
//						}
//					}
//				}
//			}
//		}
		recursive_loop(0, 0, 1000);
		
		long after = System.currentTimeMillis();
		
		long millis = after - before;	
		long second = (millis / 1000) % 60;
		long minute = (millis / (1000 * 60)) % 60;
		long hour = (millis / (1000 * 60 * 60)) % 24;
		
		String time = String.format("%02d:%02d:%02d:%d", hour, minute, second, millis%1000);
		
		System.out.println("Finished, time spend " + time + ".");
	}
}
