package org.overlaptable;

import java.io.IOException;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.mapreduce.TableMapReduceUtil;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;

public class CalcOTable {
	public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException, ParseException {
		
		CommandLineParser parser = new BasicParser();
		
		// Option Parser
		Options options = new Options();
		options.addOption("h", false, "Helps");
		options.addOption("p", true, "The Path that rtree directory located in.");
		options.addOption("i", true, "Input Path indicate the location of SSR file");
		options.addOption("o", true, "The destination table in HBase");
		options.addOption("t", true, "The type of OTable, ssr or cell");
		
		CommandLine cl = parser.parse(options, args);
		if(cl.hasOption('h')) {
			HelpFormatter f = new HelpFormatter();
			f.printHelp("OptionTips", options);
			System.exit(0);
		}
		
		String inputPath = cl.getOptionValue("i");
		String outputPath = cl.getOptionValue("o"); 
		
		Configuration conf = HBaseConfiguration.create();
		conf.set("rtreePath", cl.getOptionValue("p"));
		long milliSeconds = 1000*60*2;
		conf.setLong("mapreduce.task.timeout", milliSeconds);
		
//		conf.set("type", cl.getOptionValue("t"));
		Job job = Job.getInstance(conf, "calcOverlappingTable");
		job.setJarByClass(CalcOTable.class);

		// Add Input and Output Path
		FileInputFormat.addInputPath(job, new Path(inputPath));
		if(cl.getOptionValue("t").equals("ssr")) {
			job.setMapperClass(CalcOTableMapper.class);
		} else if(cl.getOptionValue("t").equals("cell")) {
			job.setMapperClass(CalcOTableCellMapper.class);
		} else {
			System.exit(-1);
		}
		TableMapReduceUtil.initTableReducerJob(outputPath, null, job);
		job.setNumReduceTasks(0);
		
		System.exit(job.waitForCompletion(true) ? 0 : 1);
	}
}
