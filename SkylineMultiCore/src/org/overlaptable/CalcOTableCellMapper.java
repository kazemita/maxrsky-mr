package org.overlaptable;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import mr.spatialindex.MapReduceUtility;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.HTable;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.skyline.DSLConfig;

import spatialindex.rtree.Box;
import spatialindex.rtree.Point;
import spatialindex.rtree.QueryResult;
import spatialindex.rtree.RTreePack;
import spatialindex.ssr.DSLObject;
import spatialindex.ssr.SSR;

public class CalcOTableCellMapper extends
		Mapper<LongWritable, Text, ImmutableBytesWritable, Put> {
	private static int DIM = DSLConfig.getDimension();
	private RTreePack rtpack;
	private HTable table;
	// Get the SSR of the entry
	private static byte[] fam = Bytes.toBytes("cf");
	private static byte[] dsl = Bytes.toBytes("dsl");

	@Override
	protected void setup(
			Mapper<LongWritable, Text, ImmutableBytesWritable, Put>.Context context)
			throws IOException, InterruptedException {
		// Init the RTreePack
		Configuration conf = context.getConfiguration();
		String rtreePath = conf.get("rtreePath");
		ArrayList<String> rtreeDataPaths = new ArrayList<String>();
		
		// Construct the rtree from HDFS path file
		FileSystem fs = FileSystem.get(conf);
		Path inFile = new Path(rtreePath);
		
		if (!fs.exists(inFile)) {
			  System.out.println("Input file not found");
			  System.exit(-1);
		}
		FSDataInputStream in = fs.open(inFile);
		BufferedReader br = new BufferedReader(new InputStreamReader(in));
		String line = null;
		while((line = br.readLine()) != null) {
			rtreeDataPaths.add(line);
		}
		in.close();
		br.close();
		rtpack = new RTreePack(rtreeDataPaths, DIM);
		
		// construct the overlapping table
		table = new HTable(context.getConfiguration(), "dsltable");
	}

	@Override
	protected void map(
			LongWritable key,
			Text value,
			Mapper<LongWritable, Text, ImmutableBytesWritable, Put>.Context context)
			throws IOException, InterruptedException {
		// TODO Auto-generated method stub
		String line = value.toString();

		if (line.trim().equals(""))
			return;

		// no need to construct dslObj, directly construct the cell
		Box cell = MapReduceUtility.getBoxFromString(line, DIM);
		
		System.out.println(cell.toString());
		
		QueryResult result = rtpack.query(cell);

//		// Construct BoundBox
//		Box bound = new Box(DIM);
//		for (int i = 0; i < DIM; i++) {
//			bound.lowerBounds[i] = -20000;
//			bound.upperBounds[i] = 10000;
//		}

		// examine whether the ssrs are really intersect
//		ArrayList<Long> resultIDs = result.resultIDs;
//		int size = resultIDs.size();
		StringBuilder sb = new StringBuilder();
//
//		if(size == 0)
//			return;
//		
//		System.out.println(cell.toString() + " : " + size);
//
//		int counter = 0;
//		for (int i = 0; i < size; i++) {
//			Long id = resultIDs.get(i);
//
//			Get get = new Get(Bytes.toBytes(String.valueOf(id)));
//			Result res = table.get(get);
//			String dslString = Bytes.toString(res.getValue(fam, dsl));
//
//			// Form a DSLObject and then construct its SSR
//			DSLObject olapDSL = MapReduceUtility.getPointsFromString(dslString,
//					DIM);
//			SSR olapSSR = new SSR(olapDSL, bound, DIM);
//			ArrayList<Point> interPoints = olapSSR.intersect(cell);

//			if (interPoints.size() > 0) {
//				if (counter != 0)
//					sb.append(',');
//				sb.append(id);
//				counter++;
//			}
//		}
		
		sb.append(cell.toString());
		
//		if(counter==0)
//			return;

		// insert record to hbase using format key <cell_id> - value <olapSSRs, score>
		int endIndex = line.indexOf('\t');
		String id = line.substring(0, endIndex);

		byte[] fam = Bytes.toBytes("cf");
		byte[] qut = Bytes.toBytes("olapSSRs");
		byte[] row = Bytes.toBytes(id);
		byte[] val = Bytes.toBytes(sb.toString());

		Put put = new Put(row);
		put.add(fam, qut, val);

		qut = Bytes.toBytes("score");
		val = Bytes.toBytes("0");
		put.add(fam, qut, val);

		context.write(new ImmutableBytesWritable(row), put);
	}
}
