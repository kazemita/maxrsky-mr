package org.tools;

import java.io.IOException;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.mapreduce.TableMapReduceUtil;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;

public class Import2Hbase {
	public static void main(String[] args) throws IOException, InterruptedException, ClassNotFoundException, ParseException {
		
		CommandLineParser parser = new BasicParser();
		
		// Option Parser
		Options options = new Options();
		options.addOption("i", true, "input file path in HDFS");
		options.addOption("o", true, "output hbase table name");
		options.addOption("t", true, "indicate its a cell or dsl");
		options.addOption("h", false, "Help guideline");
		
		CommandLine cl = parser.parse(options, args);
		if(cl.hasOption('h')) {
			HelpFormatter f = new HelpFormatter();
			f.printHelp("OptionTips", options);
			System.exit(0);
		}
		
		// Init the configuration file
		Configuration conf = HBaseConfiguration.create();
		String type = cl.getOptionValue("t");
		
		if(type.equals("cell")) {
			conf.set("qut", "cell");
		} else {
			conf.set("qut", "dsl");
		}
		
		Job job = Job.getInstance(conf, "ImportToHbase");
		job.setJarByClass(Import2Hbase.class);
		
		//set the input path and output path
		FileInputFormat.addInputPath(job, new Path(cl.getOptionValue("i")));
		job.setMapperClass(ImportMapper.class);
		TableMapReduceUtil.initTableReducerJob(cl.getOptionValue("o"), // output table
				null, // reducer class
				job);
		job.setNumReduceTasks(0);
		
		System.exit(job.waitForCompletion(true) ? 0 : 1);
	}
}
