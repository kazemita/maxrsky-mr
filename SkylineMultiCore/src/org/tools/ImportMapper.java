package org.tools;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class ImportMapper extends
		Mapper<LongWritable, Text, ImmutableBytesWritable, Put> {

	protected String quat;

	@Override
	protected void setup(Context context) throws IOException,
			InterruptedException {
		Configuration conf = context.getConfiguration();
		quat = conf.get("qut");
	}

	@Override
	protected void map(LongWritable key, Text value, Context context)
			throws IOException, InterruptedException {
		String line = value.toString();

		if (line.trim().equals(""))
			return;

		// extract value from each line and construct a Put record
		int endIndex = line.indexOf('\t');
		String id = line.substring(0, endIndex);		

		byte[] row = Bytes.toBytes(id);
		byte[] fam = Bytes.toBytes("cf");
		byte[] qut = Bytes.toBytes(quat);
		byte[] val = Bytes.toBytes(line);

		Put put = new Put(row);
		put.add(fam, qut, val);

		context.write(new ImmutableBytesWritable(row), put);

	}
}
