package org.skyline;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Stack;
import java.util.Vector;

import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class DSLMapper extends Mapper<LongWritable, Text, Text, Text> {
	public int DIM = DSLConfig.getDimension();
	public int POW_DIM = (1 << DIM);
	private Vector<Point> sites;

	public Vector<Point> GSKY(Vector<Point> p) {
		Vector<Point> sky = new Vector<Point>();
		Vector<Boolean> is_dominated = new Vector<Boolean>();

		for (int i = 0; i < p.size(); ++i) {
			int comp = 0;
			for (int j = 0; j < sky.size(); ++j) {
				comp = p.elementAt(i).compare(sky.elementAt(j));
				if (comp == 1)
					break;
				if (comp == -1) {
					is_dominated.set(j, Boolean.TRUE);
				}
			}
			if (comp == 1)
				continue;

			// add new skyline point and 'false' that it is not dominated (at
			// least yet).
			sky.add(p.elementAt(i));
			is_dominated.add(Boolean.FALSE);
		}

		// Remove all dominated
		Vector<Point> skyfinal = new Vector<Point>();
		for (int i = 0; i < sky.size(); ++i) {
			if (is_dominated.elementAt(i) == Boolean.FALSE)
				skyfinal.add(sky.elementAt(i));
		}

		return skyfinal;
	}

	// Non recursive traverse a Tree
	public Vector<Point> dfsTraverse(QTNode root) {
		if (root == null) {
			System.out.println("Empty Tree");
			return null;
		}

		Stack<QTNode> stack = new Stack<QTNode>();
		stack.push(root);

		Vector<Point> skylocal = new Vector<Point>();
		while (!stack.isEmpty()) {
			QTNode node = stack.pop();
			if (node.points != null) {
				skylocal.addAll(GSKY(node.points));
			}
			if (node.children != null) {
				for (int i = 0; i < node.children.length; i++) {
					QTNode child = node.children[i];
					if (child != null)
						stack.push(child);
				}
			}
		}

		return skylocal;
	}

	@Override
	protected void setup(Context context) throws IOException {
		FileSystem fs = FileSystem.get(context.getConfiguration());
		Path path = new Path(DSLConfig.getPath());
		BufferedReader br = new BufferedReader(new InputStreamReader(
				fs.open(path)));

		String line = "";
		sites = new Vector<Point>();
		int count = 0;
		while ((line = br.readLine()) != null) {
			count++;
			Point p = new Point();
			if (!p.parseFromRawLine(line)) {
				System.out.println("Error in input file sites, line " + count);
				continue;
			}
			sites.add(p);
		}
		br.close();
		System.out.println("DONE setup");

	}

	@Override
	public void map(LongWritable key, Text value, Context context)
			throws IOException, InterruptedException {
		String line = value.toString();

		if (line.trim().equals(""))
			return;

		// parse the input line according to the format ID\tPoint
		String[] tokens = line.split("\t");
		String id = tokens[0];
		String pline = tokens[1];

		Point object = new Point();
		object.parseFromRawLine(pline);

		Vector<Point> newSite = new Vector<Point>();
		for (Point p : sites) {
			Point newP = new Point(p);
			newP.subtract(object);
			newSite.add(newP);
		}

		// long start = System.currentTimeMillis();
		//
		// double[] low = new double[DIM];
		// double[] high = new double[DIM];
		//
		// for (int i = 0; i < DIM; i++) {
		// low[i] = -1;
		// high[i] = 10;
		// }
		//
		// QTNode root = new QTNode(0, "", newSite, low, high);
		// root.divide(200);
		//
		// long end = System.currentTimeMillis();
		// System.out.println("QTree: " + (end - start) + "ms");
		//
		// Vector<Point> skyline = GSKY(dfsTraverse(root));

		Vector<Point> skyline = GSKY(newSite);
		// end = System.currentTimeMillis();
		// System.out.println("GSKY: " + (end - start) + "ms");
		// format the output
		// String outValue = "";
		// for (Point p : skyline) {
		// for(int counter = 0; counter < POW_DIM; counter++) {
		// if(counter > 0)
		// outValue += ";";
		// outValue += p.transfer(counter);
		// }
		// }

		// String outValue = "";
		// for (int i = 0; i < skyline.size(); i++) {
		// if (i > 0)
		// outValue += ";";
		// Point p = skyline.elementAt(i);
		// outValue += p.toString();
		// }

		StringBuilder outValue = new StringBuilder();
		outValue.append(object.toString());
		outValue.append("@");
		int i;
		for (i = 0; i < skyline.size() - 1; i++) {
			outValue.append(skyline.elementAt(i).toString());
			// skyline.elementAt(i).toString(outValue);
			outValue.append(";");
		}
		outValue.append(skyline.elementAt(i).toString());
		// skyline.elementAt(i).toString(outValue);

//		context.write(NullWritable.get(), new Text(outValue.toString()));
		context.write(new Text(id), new Text(outValue.toString()));

	}

}
