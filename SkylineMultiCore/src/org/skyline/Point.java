package org.skyline;

import java.text.DecimalFormat;

public class Point {

	public static final int DIM = DSLConfig.getDimension();

	public float[] value;

	public Point() {
		value = new float[DIM];
	}

	public Point(Point p) {
		value = new float[DIM];
		for (int i = 0; i < value.length; i++) {
			value[i] = p.value[i];
		}
	}

	public boolean subtract(Point p) {
		if (this.value.length != p.value.length) {
			return false;
		} else {
			for (int i = 0; i < value.length; i++) {
				value[i] = Math.abs(value[i] - p.value[i]);
			}
			return true;
		}
	}

	public void parseFromString(String line) {
		String delims = ",";
		String[] tokens = line.split(delims);
		for (int i = 0; i < value.length; i++) {
			value[i] = Float.parseFloat(tokens[i]);
		}
	}

	// returns true if 'this' dominates (MIN) 'p'.
	public boolean dominates(Point p) {
		boolean less_found = false;
		for (int i = 0; i < value.length; ++i) {
			if (value[i] > p.value[i])
				return false;
			if (value[i] < p.value[i])
				less_found = true;
		}
		return less_found;
	}

	// returns
	// -1 this dominates p
	// 0 incomparable (no point dominates the other)
	// 1 p dominates this
	public int compare(Point p) {
		int this_is_less_than_p = 0;
		int p_is_less_than_this = 0;
		for (int i = 0; i < value.length; ++i) {
			if (value[i] > p.value[i])
				p_is_less_than_this = 1;
			else if (value[i] < p.value[i])
				this_is_less_than_p = -1;
		}
		return p_is_less_than_this + this_is_less_than_p;
	}

	public boolean parseFromRawLine(String line) {
		// TODO Auto-generated method stub
		if (line.equals(""))
			return false;

		String delims = ",";
		String[] tokens = line.split(delims);

		for (int i = 0; i < value.length; ++i) {
			value[i] = Float.parseFloat(tokens[i]);
		}
		return true;
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		DecimalFormat df = new DecimalFormat("#.000");
		int i;
		for (i = 0; i < value.length - 1; i++) {
			sb.append(df.format(value[i]));
			sb.append(",");
		}
		sb.append(df.format(value[i]));
		return sb.toString();
	}
	
	public void toString(StringBuilder sb) {
		int i;
		for (i = 0; i < value.length - 1; i++) {
			sb.append(Double.valueOf(value[i]));
			sb.append(",");
		}
		sb.append(Double.valueOf(value[i]));
	}
	
	public String transfer(int counter) {
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i< DIM; i++) {
			double val = value[i];
			if(i > 0)
				sb.append(",");
			if((counter & (1 << i)) != 0) {
				val = -val;
			}
			DecimalFormat df = new DecimalFormat("0.00##############");
			sb.append(Double.valueOf(df.format(val)));
		}
		return sb.toString();
	}
}
