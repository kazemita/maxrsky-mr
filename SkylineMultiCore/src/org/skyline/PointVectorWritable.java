package org.skyline;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Vector;

import org.apache.hadoop.io.Writable;

public class PointVectorWritable implements Writable {
	private int DIM = DSLConfig.getDimension();
	private Vector<Point> value;
	
	public PointVectorWritable() {
		value = new Vector<Point>();
	}
	
	public PointVectorWritable(Vector<Point> value) {
		set(value);
	}
	

	public void set(Vector<Point> value) {
		this.value = value;
	}
	
	public Vector<Point> get() {
		return this.value;
	}

	@Override
	public void readFields(DataInput in) throws IOException {
		// TODO Auto-generated method stub
		in.readDouble();
	}

	@Override
	public void write(DataOutput out) throws IOException {
		int i;
		for (i = 0; i < value.size() -1; i++) {
			Point p = value.elementAt(i);
			int j;
			for (j = 0; j < DIM - 1; j++) {
				out.writeDouble(p.value[j]);
				out.writeChar(44);
			}
			out.writeDouble(p.value[j]);
			out.writeChar(59);
		}
		Point p = value.elementAt(i);
		int j;
		for(j=0; j<DIM - 1; j++) {
			out.writeDouble(p.value[j]);
			out.writeChar(44);
		}
		out.writeDouble(p.value[j]);
	}

}
