package org.skyline;

import java.util.ArrayList;
import java.util.Vector;

public class QTNode {
	public int depth;
	public String id;
	public QTNode[] children;

	public Vector<Point> points;
	public double[] low;
	public double[] high;

	public static final String DIM_SIZE_STRING = DSLConfig.getDimSizeString();
	public static final int DIM = DSLConfig.getDimension();
	public static final int TWO_TO_THE_POWER_DIM = (1 << DIM); // 8

	public QTNode(int depth, String id, Vector<Point> points, double[] low,
			double[] high) {
		this.depth = depth;
		this.id = id;
		this.children = null;
		this.points = points;
		this.low = low;
		this.high = high;

		if (id.length() != depth * DIM)
			throw new RuntimeException("|id| != depth*DIM");
	}

	// for example, DIM=5, num=13 >>> 01101
	public String numToOneDepthId(int num) {
		StringBuilder sb = new StringBuilder(DIM_SIZE_STRING);
		for (int i = DIM - 1; i >= 0; --i) {
			sb.setCharAt(i, (char) ('0' + num % 2));
			num /= 2;
		}
		return sb.toString();
	}

	// Returns -1 if this is a leaf node (i.e. no childs).
	// Returns the child index in which this point will go.
	public int childIndex(Point p) {
		if (children == null)
			return -1;

		int chi = 0;
		for (int i = 0; i < DIM; i++) {
			if (p.value[i] >= (low[i] + high[i]) / 2) {
				chi |= (1 << i);
			}
		}
		return chi;
	}

	// maxp = threshold count for dividing QuadTrees.
	public void divide(int maxp) {
		if (points.size() <= maxp)
			return;

		children = new QTNode[TWO_TO_THE_POWER_DIM];
		ArrayList<Vector<Point>> chi_points = new ArrayList<Vector<Point>>(
				TWO_TO_THE_POWER_DIM);

		double chi_low[][] = new double[TWO_TO_THE_POWER_DIM][DIM];
		double chi_high[][] = new double[TWO_TO_THE_POWER_DIM][DIM];

		for (int chi = 0; chi < TWO_TO_THE_POWER_DIM; chi++) {
			chi_points.add(new Vector<Point>());
			for (int i = 0; i < DIM; i++) {
				if ((chi & (1 << i)) == 0) {
					chi_low[chi][i] = low[i];
					chi_high[chi][i] = (low[i] + high[i]) / 2;
				} else {
					chi_low[chi][i] = (low[i] + high[i]) / 2;
					chi_high[chi][i] = high[i];
				}
			}
		}

		// move point to appropriate child
		for (Point p : points) {
			chi_points.get(childIndex(p)).add(p);
		}

		// create child QTNode's.
		for (int chi = 0; chi < TWO_TO_THE_POWER_DIM; ++chi) {
			children[chi] = new QTNode(depth + 1, id + numToOneDepthId(chi),
					chi_points.get(chi), chi_low[chi], chi_high[chi]);
		}

		// remove points from this.
		points = null;

		// prune 11...1 if 00...0 has at least one point.
		if (children[0].points.size() > 0) {
			children[TWO_TO_THE_POWER_DIM - 1] = null;
		}

		// call this recursively on each child.
		for (int chi = 0; chi < TWO_TO_THE_POWER_DIM; ++chi) {
			if (children[chi] != null) {
				children[chi].divide(maxp);
			}
		}
	}

	// Returns
	// < 0 if sub1 DOMINATES sub2
	// = 0 if same [i.e. substring[0, minimums length] matches.
	// > 0 if sub2 DOMINATES sub1
	// Examples
	// "" == 010011 (anything)
	// 0 == 0
	// 0 < 1
	// 0 == 01
	// 11 == 1100
	// 11 > 101
	// 11 > 101
	// 10 > 01
	public int compare(String sub1, String sub2) {
		int ml = Math.min(sub1.length(), sub2.length());
		return sub1.substring(0, ml).compareTo(sub2.substring(0, ml));
	}

	private String sub(int k) { // 0 <= k < DIM
		StringBuilder sb = new StringBuilder();
		for (int i = k; i < id.length(); i += DIM) {
			sb.append(id.charAt(i));
		}
		return sb.toString();
	}

	// Returns true if this dominates q.
	public boolean dominates(QTNode q) {
		boolean less_found = false;
		for (int k = 0; k < DIM; ++k) {
			int cmp = compare(sub(k), q.sub(k));
			if (cmp > 0)
				return false;
			if (cmp < 0)
				less_found = true;
		}
		return less_found;
	}

}
