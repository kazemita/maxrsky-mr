package org.skyline;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.map.MultithreadedMapper;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class DSL {
	public static void main(String[] args) throws IOException,
			InterruptedException, ClassNotFoundException {
		if (args.length != 2) {
			System.err.println("Usage: DSL <input path> <outputpath>");
			System.exit(-1);
		}

		Configuration conf = new Configuration();
//		conf.setBoolean("mapreduce.map.output.compress", true);
//		conf.setClass("mapreduce.map.output.compress.codec", GzipCodec.class, CompressionCodec.class);
		conf.setInt("mapred.max.split.size", 33554432);

		Job job = Job.getInstance(conf);
		job.setJarByClass(DSL.class);
		job.setJobName("Dynamic Skyline");

		FileInputFormat.addInputPath(job, new Path(args[0]));
		FileOutputFormat.setOutputPath(job, new Path(args[1]));
		
//		job.setMapperClass(DSLMapper.class);
		job.setMapperClass(MultithreadedMapper.class);
		MultithreadedMapper.setMapperClass(job, DSLMapper.class);
		MultithreadedMapper.setNumberOfThreads(job, 2);
		job.setNumReduceTasks(0);

		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(Text.class);
//		FileOutputFormat.setCompressOutput(job, true);
//		FileOutputFormat.setOutputCompressorClass(job, GzipCodec.class);

		System.exit(job.waitForCompletion(true) ? 0 : 1);

	}
}
