package org.skyline;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

public class DSLConfig {

	private static ResourceBundle resource = ResourceBundle.getBundle("config");

	public static ResourceBundle setup() {
		String proFilePath = System.getProperty("user.dir")
				+ "/config.properties";
		ResourceBundle resource = null;
		try {
			BufferedInputStream inputStream = new BufferedInputStream(
					new FileInputStream(proFilePath));
			resource = new PropertyResourceBundle(inputStream);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return resource;
	}

	public static int getDimension() {
		String dim = resource.getString("dimension");
		return Integer.parseInt(dim);
	}

	public static String getPath() {
		return resource.getString("path");
	}

	public static String getLocalQtdPath() {
		return resource.getString("path.local.quadtree");
	}

	public static String getSamplePath() {
		return resource.getString("path.local.sample");
	}

	public static String getHdfsQtdPath() {
		return resource.getString("path.hdfs.qtd");
	}

	public static String getHdfsQtdPathWithPrefix() {
		return resource.getString("path.hdfs.qtd.prefix");
	}

	public static String getHdfsVpnPath() {
		return resource.getString("path.hdfs.vpn");
	}

	public static String getHdfsVpnPathWithPrefix() {
		return resource.getString("path.hdfs.vpn.prefix");
	}

	public static String getHdfsFilterPath() {
		return resource.getString("path.hdfs.filter");
	}

	public static String getHdfsFilterPathWithPrefix() {
		return resource.getString("path.hdfs.filter.prefix");
	}

	public static String getDimSizeString() {
		return resource.getString("dimension.size.string");
	}

}
