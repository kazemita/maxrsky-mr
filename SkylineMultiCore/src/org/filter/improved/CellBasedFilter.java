package org.filter.improved;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.mapreduce.TableMapReduceUtil;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.mapreduce.Job;
import org.filter.baseline.MaxRQueryResultWritable;

public class CellBasedFilter {
	public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException {
		Configuration conf = HBaseConfiguration.create();
		Job job = Job.getInstance(conf, "ImprovedMethod");
		job.setJarByClass(CellBasedFilter.class);

		Scan scan = new Scan();
		scan.setCaching(500);
		scan.setCacheBlocks(false);

		// Set the configuration of Mapper
		TableMapReduceUtil.initTableMapperJob("overlaptable", scan,
				CellBasedFilterMapper.class, NullWritable.class,
				MaxRQueryResultWritable.class, job);
		// Set the configuration of Reducer
		
		boolean b = job.waitForCompletion(true);
		if (!b) {
		    throw new IOException("error with job!");
		}
		
	}
}
