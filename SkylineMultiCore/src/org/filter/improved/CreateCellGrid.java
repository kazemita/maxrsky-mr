package org.filter.improved;

import java.io.IOException;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.skyline.DSLConfig;

import spatialindex.rtree.Box;

public class CreateCellGrid {
	protected static FSDataOutputStream out; 
	protected static int offset;
	protected static float lbound[];
	protected static float hbound[];
	protected static int DIM = DSLConfig.getDimension();
	protected static int counter = 0;
	protected static int progress = 0;
	protected static Box box;
	
	public static void recursive_loop(int depth, int min, int max) throws IOException {
		for (int i = min; i < max; i += offset) {
			if(depth == 0) {
				System.out.println("%" + progress++);
			}
			if(depth < DIM - 1) {
				lbound[depth] = i;
				recursive_loop(depth + 1, min, max);
			} else {
				lbound[depth] = i;
				for(int j = 0; j < DIM; j++) {
					hbound[j] = lbound[j] + offset;
				}
				box.id = ++counter;
				box.set(lbound, hbound);
				byte[] byt = box.toStringWithLineBreak().getBytes();
				out.write(byt);
			}
		}
	}
	
	public static void main(String[] args) throws IOException, ParseException {
		
		CommandLineParser parser = new BasicParser();
		
		// Option Parser
		Options options = new Options();
		options.addOption("max", true, "Max value in a DIM.");
		options.addOption("min", true, "Min value in a DIM.");
		options.addOption("o", true, "The output path in HDFS.");
		options.addOption("n", true, "The number of slice we want to divide for each DIM.");
		options.addOption("h", false, "Help Guide");
		
		CommandLine cl = parser.parse(options, args);
		if(cl.hasOption('h')) {
			HelpFormatter f = new HelpFormatter();
			f.printHelp("OptionTips", options);
			System.exit(0);
		}
		
		Configuration conf = new Configuration();
//		conf.addResource(new Path("core-site.xml"));
//		conf.addResource(new Path("core-hdfs.xml"));
		FileSystem fs = FileSystem.get(conf);
		
		Path outFile = new Path(cl.getOptionValue("o"));
		if(fs.exists(outFile)) {
			System.out.println("Output file already exists!");
			System.exit(-1);
		}
		
		out = fs.create(outFile);
		
		int max = Integer.parseInt(cl.getOptionValue("max"));
		int min = Integer.parseInt(cl.getOptionValue("min"));
		int num = Integer.parseInt(cl.getOptionValue("n"));
		offset = (max - min) / num;
		lbound = new float[DIM];
		hbound = new float[DIM];
		box = new Box(DIM);
		
		System.out.println("====== start generate data ======");
		recursive_loop(0, min, max);
		System.out.println("====== finish generate data ======");
		
		out.close();
	}
}
