package org.filter.baseline;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.hadoop.io.Writable;

public class MaxRQueryResultWritable extends MaxRQueryResult implements Writable {
	
	public MaxRQueryResultWritable(String q, ArrayList<Long> olapssrs) {
		// TODO Auto-generated constructor stub
		super(q, olapssrs);
	}

	@Override
	public void readFields(DataInput in) throws IOException {
		// TODO Auto-generated method stub
		int size = in.readInt();
		olapssrs = new ArrayList<Long>(size);
		
		for(int i = 0; i < size; i++) {
			Long elem = in.readLong();
			olapssrs.add(elem);
		}
		
		q = in.readLine();
	}

	@Override
	public void write(DataOutput out) throws IOException {
		
		out.writeInt(olapssrs.size());
		for(Long elem:olapssrs) {
			out.writeLong(elem);
		}
		
		out.writeChars(q);
	}

}
