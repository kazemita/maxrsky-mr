package org.filter.baseline;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import mr.spatialindex.MapReduceUtility;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.HTable;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableMapper;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.NullWritable;
import org.skyline.DSLConfig;

import spatialindex.rtree.Box;
import spatialindex.rtree.Point;
import spatialindex.rtree.RTreePack;
import spatialindex.ssr.DSLObject;
import spatialindex.ssr.SSR;

public class BaselineMapper extends
		TableMapper<NullWritable, MaxRQueryResultWritable> {
	private static int DIM = DSLConfig.getDimension();
	private HTable table = null;
	private RTreePack rtpack;
	private int max_score;
	private Box bound;
	// Get the SSR of the entry
	private static byte[] fam = Bytes.toBytes("cf");
	private static byte[] dsl = Bytes.toBytes("dsl");
	private static byte[] score = Bytes.toBytes("score");
	private static byte[] olapSSRs = Bytes.toBytes("olapSSRs");

	@Override
	protected void setup(Context context) throws IOException,
			InterruptedException {
		// construct the overlapping table
		Configuration conf = context.getConfiguration();
		table = new HTable(conf, "dsltable");
		
		// Init the RTreePack
		String rtreePath = conf.get("rtreePath");
		ArrayList<String> rtreeDataPaths = new ArrayList<String>();

		// Construct the rtree from HDFS path file
		FileSystem fs = FileSystem.get(conf);
		Path inFile = new Path(rtreePath);

		if (!fs.exists(inFile)) {
			System.out.println("Input file not found");
			System.exit(-1);
		}
		FSDataInputStream in = fs.open(inFile);
		BufferedReader br = new BufferedReader(new InputStreamReader(in));
		String line = null;
		while ((line = br.readLine()) != null) {
			rtreeDataPaths.add(line);
		}
		in.close();
		br.close();
		
		rtpack = new RTreePack(rtreeDataPaths, DIM);
		max_score = 0;

		// Construct the SSR from the candidate intersect mbr box
		bound = new Box(DIM);
		for (int i = 0; i < DIM; i++) {
			bound.lowerBounds[i] = -20000;
			bound.upperBounds[i] = 10000;
		}
	}

	@Override
	public void map(ImmutableBytesWritable row, Result value, Context context)
			throws IOException, InterruptedException {

		byte[] sc = value.getValue(fam, score);

		if (Bytes.toInt(sc) < max_score)
			return;

		context.write(NullWritable.get(),
				resultToPut(row, value, table, rtpack));
	}

	private MaxRQueryResultWritable resultToPut(ImmutableBytesWritable key,
			Result result, HTable table, RTreePack rtpack) throws IOException {
		// Retrieve the entry record from the hbase table
		Get get = new Get(key.get());
		Result res = table.get(get);
		byte[] val = res.getValue(fam, dsl);

		// Form a DSLObject and then construct its SSR
		DSLObject entryDSL = MapReduceUtility.getPointsFromString(
				Bytes.toString(val), DIM);
		SSR entrySSR = new SSR(entryDSL, bound, DIM);

		/*
		 * Extracts the SSR Ids and then for each SSR find its Max Intersects
		 * Region
		 */
		byte[] ossrIds = result.getValue(fam, olapSSRs);
		String idStr = Bytes.toString(ossrIds);
		String[] OSSRIds = idStr.split(",");

		// TODO: The score dealing

		ArrayList<Long> resltIds = new ArrayList<Long>();
		Point maxq = null;
		for (String id : OSSRIds) {
			get = new Get(Bytes.toBytes(id));
			res = table.get(get);
			val = res.getValue(fam, dsl);

			// Form a DSLObject and then construct its SSR
			DSLObject olapDSL = MapReduceUtility.getPointsFromString(
					Bytes.toString(val), DIM);
			SSR olapSSR = new SSR(olapDSL, bound, DIM);
			ArrayList<Point> interPoints = entrySSR.intersectImproved(olapSSR);

			// TODO: Consider a situation that the size is 0
			for (Point p : interPoints) {
				// Point Query is Accurate?
				ArrayList<Long> retIds = rtpack.query(p).resultIDs;
				if (retIds.size() > resltIds.size()) {
					maxq = p;
					resltIds = retIds;
				}
			}
		}

		int size = resltIds.size();

		if (max_score < size)
			max_score = size;

		return new MaxRQueryResultWritable(maxq.toString(), resltIds);

		/*
		 * Put put = new Put(key.get());
		 * 
		 * System.out.println("start working..."); fam =
		 * Bytes.toBytes("colfam1"); byte[] score = result.getValue(fam,
		 * Bytes.toBytes("score")); int sc =
		 * Integer.parseInt(Bytes.toString(score));
		 * 
		 * System.out.println("Print SSRs overlapping with " +
		 * Bytes.toString(key.get()) + "...");
		 * 
		 * for (int i = 0; i < sc; i++) { byte[] skey = result.getValue(fam,
		 * Bytes.toBytes("olapr-" + (i + 1))); get = new Get(skey); res =
		 * table.get(get); for (KeyValue kv : res.raw()) {
		 * System.out.println("Row: " + Bytes.toString(kv.getRow()) + " Value: "
		 * + Bytes.toString(kv.getValue())); }
		 * 
		 * // ADD Calculate intersection Point here
		 * 
		 * }
		 * 
		 * // For every intersection Point, find its corresponding in R-tree
		 * 
		 * for (KeyValue kv : result.raw()) { put.add(kv); } return put;
		 */
	}
}
