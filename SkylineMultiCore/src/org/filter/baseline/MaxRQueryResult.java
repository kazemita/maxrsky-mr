package org.filter.baseline;

import java.util.ArrayList;

public class MaxRQueryResult implements Comparable<MaxRQueryResult> {
	protected String q;
	protected ArrayList<Long> olapssrs;
	
	public MaxRQueryResult(String q, ArrayList<Long> olapssrs) {
		this.q = q;
		this.olapssrs = olapssrs;
	}
	
	public void setValue(String q, ArrayList<Long> olapssrs) {
		this.q = q;
		this.olapssrs = olapssrs;
	}
	
	public int size() {
		return olapssrs.size();
	}

	@Override
	public int compareTo(MaxRQueryResult o) {
		return Integer.compare(size(), o.size());
	}
	
	// Convert to human readable String
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append(q);
		sb.append(":[");
		for (int i = 0; i < olapssrs.size(); i++) {
			if (i != 0)
				sb.append(", ");
			sb.append(olapssrs.get(i));
		}
		sb.append("]");

		return sb.toString();
	}
 
}
