package org.filter.baseline;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.mapreduce.TableMapReduceUtil;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.mapreduce.Job;

public class Baseline {
	public static void main(String[] args) throws IOException, InterruptedException, ClassNotFoundException {
		Configuration conf = HBaseConfiguration.create();
		Job job = Job.getInstance(conf, "BaselineMethod");
		job.setJarByClass(Baseline.class);

		Scan scan = new Scan();
		scan.setCaching(500);
		scan.setCacheBlocks(false);
		
		TableMapReduceUtil.initTableMapperJob("overlaptable", // input table
				scan, // Scan instance to control CF and attribute selection
				BaselineMapper.class, // mapper class
				NullWritable.class, // mapper output key
				MaxRQueryResultWritable.class, // mapper output value
				job);
		TableMapReduceUtil.initTableReducerJob("maxrquery", // output table
				BaselineReducer.class, // reducer class
				job);
		job.setNumReduceTasks(1);
				
		boolean b = job.waitForCompletion(true);
		if (!b) {
		    throw new IOException("error with job!");
		}

		// FileInputFormat.addInputPath(job, new Path(args[0]));
		// FileOutputFormat.setOutputPath(job, new Path(args[1]));
		//
		// job.setMapperClass(BasicFilterMapper.class);
	}
}
