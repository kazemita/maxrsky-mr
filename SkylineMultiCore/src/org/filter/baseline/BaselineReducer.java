package org.filter.baseline;

import java.io.IOException;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableReducer;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.NullWritable;

public class BaselineReducer extends TableReducer<NullWritable, MaxRQueryResultWritable, ImmutableBytesWritable>{
	public static final byte[] CF = "cf".getBytes();
	public static final byte[] RES = "result".getBytes();
	
	@Override
	protected void reduce(
			NullWritable key,
			Iterable<MaxRQueryResultWritable> values,
			Context context)
			throws IOException, InterruptedException {
		MaxRQueryResultWritable maxRes = null;
		for(MaxRQueryResultWritable res:values) {
			if(res.compareTo(maxRes) > 0) {
				maxRes = res;
			}
		}
		
		Put put = new Put(Bytes.toBytes("res"));
		put.add(CF, RES, Bytes.toBytes(maxRes.toString()));
		
		context.write(null, put);
	}

}
